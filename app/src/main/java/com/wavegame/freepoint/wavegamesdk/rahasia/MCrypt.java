package com.wavegame.freepoint.wavegamesdk.rahasia;

import android.util.Base64;

import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MCrypt {

    static char[] HEX_CHARS = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};


    private IvParameterSpec ivspec;
    private SecretKeySpec keyspec;
    private Cipher cipher;

    private String iv = "keU5sox7HvCkw8Lb";
    private String SecretKey = "plaintext data goes here";
    public MCrypt()
    {
        ivspec = new IvParameterSpec(iv.getBytes());

        keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES/CBC/PKCS5Padding");

        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public byte[] encrypt(String text) throws Exception
    {
        if(text == null || text.length() == 0)
            throw new Exception("Empty string");

        byte[] encrypted = null;

        try {
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);

            encrypted = cipher.doFinal(padString(text).getBytes());
        } catch (Exception e)
        {
            throw new Exception("[encrypt] " + e.getMessage());
        }

        return encrypted;
    }

    public byte[] decrypt(String code) throws Exception
    {
        if(code == null || code.length() == 0)
            throw new Exception("Empty string");

        byte[] decrypted = null;

        try {
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            decrypted = cipher.doFinal(hexToBytes(code));
            //Remove trailing zeroes
            if( decrypted.length > 0)
            {
                int trim = 0;
                for( int i = decrypted.length - 1; i >= 0; i-- ) if( decrypted[i] == 0 ) trim++;

                if( trim > 0 )
                {
                    byte[] newArray = new byte[decrypted.length - trim];
                    System.arraycopy(decrypted, 0, newArray, 0, decrypted.length - trim);
                    decrypted = newArray;
                }
            }
        } catch (Exception e)
        {
            throw new Exception("[decrypt] " + e.getMessage());
        }
        return decrypted;
    }

    public static byte[] PublicKeyEncrypt(String plainText) {

        String publicKey  = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0Fxfe4IB6k8NPSxKxZFe\n";
        publicKey += "t5apGAVEDt/okSB8dPZ2s8gfX0zJJsyVl9TPWAkzrFJEUj6FxA014ch7DQykarHc\n";
        publicKey += "Pb35d7ct7ak5ct1Y52Wrzkvo8N0uLPWJBt6wxPOfhPUp5oTqija62Zylalqz8cjZ\n";
        publicKey += "w9n2PtNoKSOdcogFIs9KTrVUP4v6R2rbbT+8DYhKfUADMOENNyzbB/Y8+W9PMhAt\n";
        publicKey += "XAxmYmxaDJXGkLiI/yU+nuKbPQBLJOmK9gxTiIiMvVbGgfVCmPUONgMcCfSHzL6J\n";
        publicKey += "gVGmEXE7AgFt/curkPcULnV9ab5UO0dfJJCeLXz56TAUkOQqa2Z+3eSDPezPZiwZ\n";
        publicKey += "1QIDAQAB\n";
        byte[] encrypted=null;
        byte[] keyBytes = Base64.decode(publicKey, Base64.DEFAULT);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        String encryptedEncoded = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey key = keyFactory.generatePublic(spec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encrypted = cipher.doFinal(padString(plainText).getBytes());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


        return encrypted;
    }


    public static String bytesToHex(byte[] buf)
    {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i)
        {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }


    public static byte[] hexToBytes(String str) {
        if (str==null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i=0; i<len; i++) {
                buffer[i] = (byte) Integer.parseInt(str.substring(i*2,i*2+2),16);
            }
            return buffer;
        }
    }



    private static String padString(String source)
    {
        char paddingChar = 0;
        int size = 16;
        int x = source.length() % size;
        int padLength = size - x;

        for (int i = 0; i < padLength; i++)
        {
            source += paddingChar;
        }

        return source;
    }

    public byte[] getEncryptedAES(){
        return PublicKeyEncrypt(SecretKey);
    }

    public String getIv(){
        return iv;
    }
}
