package com.wavegame.freepoint.wavegamesdk.restmanager;

import com.wavegame.freepoint.wavegamesdk.callback.AppItemService;
import com.wavegame.freepoint.wavegamesdk.helper.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by wavegame9 on 2/25/2016.
 */
public class AppItemRestManager {

    private AppItemService mAppItemService;

    public AppItemService getAppItemService() {

        if(mAppItemService == null)
        {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HTTP.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            mAppItemService = retrofit.create(AppItemService.class);
        }

        return mAppItemService;
    }
}
