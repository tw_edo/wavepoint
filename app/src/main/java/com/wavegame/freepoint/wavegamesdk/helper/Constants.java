package com.wavegame.freepoint.wavegamesdk.helper;

import android.util.Base64;

/**
 * Created by wavegame9 on 2/25/2016.
 */
public class Constants {

    public static final class HTTP{

        public static final String BASE_URL = "http://catfiz.com:8005";
        public static final String LIST_APP_URL = "/dlfizzlink?fl=x0BrSPnluYz3thZG&user=7F8C";
        public static final int BASE64_MODE =  Base64.NO_WRAP;
        public static final int BASE64_DEFAULT =  Base64.DEFAULT;
    }

    public static final class PREFERENCE {
        public static final String MPREFERENCES = "FPPrefs" ;
        public static final String PROFILE_NAME = "PROFILE_NAME";
        public static final String PROFILE_ID = "PROFILE_ID";
        public static final String PROFILE_PIC_URI = "PROFILE_PIC_URI";
    }
}
