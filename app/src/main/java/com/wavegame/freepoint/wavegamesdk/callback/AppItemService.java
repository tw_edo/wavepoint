package com.wavegame.freepoint.wavegamesdk.callback;

import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.model.AppItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by wavegame9 on 2/25/2016.
 */
public interface AppItemService {

    @GET(Constants.HTTP.LIST_APP_URL)
    Call<List<AppItem>> getPremiumApps();
}
