package com.wavegame.freepoint.wavegamesdk.rahasia;


import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.Base64;

import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;


import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

//CLASS UNTUK ENCRYPSI DAN DECRYPT AES
public class Security {
    public static final String TAG = Security.class.getSimpleName();

    private static final int PBE_ITERATION_COUNT = 100;
    private static final int SALT_LENGTH = 20;

    private static final String SECRET_KEY_ALGORITHM = "AES";
    private static final String PROVIDER = "BC";
    private static final String PBE_ALGORITHM = "PBEWithSHA256And256BitAES-CBC-BC";
    private static final String RANDOM_ALGORITHM = "SHA1PRNG";
    private static Charset CHARSET = Charset.forName("US-ASCII");
    public static final int IV_LENGTH = 16;

//    public static String encrypt(String input, String key)throws Exception{
//        java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//        byte[] crypted = null;
//        try{
//            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");
//            cipher.init(Cipher.ENCRYPT_MODE, skey);
//
//            crypted = cipher.doFinal(input.getBytes());
//        }catch(Exception e){
//            Log.d(TAG, e.toString());
//        }
//        return new String(Base64.encode(crypted, Constants.HTTP.BASE64_MODE), "UTF-8");
//    }

    public static String encryptCBC(Key aesKey, byte[] iv, String input) throws Exception {
        String encryptedText = null;
        try {
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            Cipher encryptionCipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            encryptionCipher.init(Cipher.ENCRYPT_MODE, aesKey, ivspec);
            byte[] encrypted = encryptionCipher.doFinal(input.getBytes());

            encryptedText = new String(Base64.encode(encrypted, Base64.NO_PADDING),"UTF-8");
        } catch (Exception e) {
            throw new Exception("Unable to encrypt", e);
        }
        return encryptedText;
    }

    public static String decryptCBC(String randomSecretKey, String ivEncoded, String encryptedTextEncoded) throws Exception {
        Key aesKey = getAESKey(randomSecretKey);
        //Key aesKey = getAESKey(PublicKeyDecrypt.show(randomSecretKey));
        byte[] iv = Base64.decode(ivEncoded.getBytes("UTF-8"), Constants.HTTP.BASE64_MODE);
        byte[] encryptedByte = Base64.decode(encryptedTextEncoded.getBytes("UTF-8"), Constants.HTTP.BASE64_MODE);
        String encryptedText = null;
        try {
            IvParameterSpec ivSpec = new IvParameterSpec(iv);
            Cipher cipheer = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipheer.init(Cipher.DECRYPT_MODE, aesKey, ivSpec);
            byte[] encryptedTextAes = null;
            encryptedTextAes = cipheer.doFinal(encryptedByte);
            encryptedText = new String(encryptedTextAes, "UTF-8");
        } catch (Exception e) {
            throw new Exception("Unable to encrypt", e);
        }
        return encryptedText;
    }

//    public static String decrypt(String input, String key)throws Exception{
//        java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//        byte[] output = null;
//        try{
//            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "BC");
//            cipher.init(Cipher.DECRYPT_MODE, skey);
//            output = cipher.doFinal(Base64.decode(input, Constants.HTTP.BASE64_MODE));
//        }catch(Exception e){
//            System.out.println(e.toString());
//        }
//        return new String(output);
//    }

    public static SecretKey getSecretKey(String password, String salt) throws Exception {
        try {
            PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray(), toByte(salt), PBE_ITERATION_COUNT, 256);
            SecretKeyFactory factory = SecretKeyFactory.getInstance(PBE_ALGORITHM, PROVIDER);
            SecretKey tmp = factory.generateSecret(pbeKeySpec);
//            SecretKey secret = new SecretKeySpec(tmp.getEncoded(), SECRET_KEY_ALGORITHM);
//            return secret;
            return tmp;
        } catch (Exception e) {
            throw new Exception("Unable to get secret key", e);
        }
    }

    public static Key getAESKey(String generatedSecret){
        Key secret = new SecretKeySpec(Base64.decode(generatedSecret.getBytes(), Constants.HTTP.BASE64_MODE), SECRET_KEY_ALGORITHM);
        return secret;
    }

    public static byte[] toByte(String hexString) {
        int len = hexString.length()/2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
        return result;
    }

    public static String toHex(byte[] buf) {
        if (buf == null)
            return "";
        StringBuffer result = new StringBuffer(2*buf.length);
        for (int i = 0; i < buf.length; i++) {
            appendHex(result, buf[i]);
        }
        return result.toString();
    }

    private final static String HEX = "0123456789ABCDEF";
    private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b>>4)&0x0f)).append(HEX.charAt(b&0x0f));
    }

    public String generateSalt() throws Exception {
        try {
            SecureRandom random = SecureRandom.getInstance(RANDOM_ALGORITHM);
            byte[] salt = new byte[SALT_LENGTH];
            random.nextBytes(salt);
            String saltHex = toHex(salt);
            return saltHex;
        } catch (Exception e) {
            throw new Exception("Unable to generate salt", e);
        }
    }

    public static byte[] generateIv() throws NoSuchAlgorithmException, NoSuchProviderException {
        char[] CHARSET_AZ_09 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        String initialVectorString = randomString(CHARSET_AZ_09, 16);
        //SecureRandom random = SecureRandom.getInstance(RANDOM_ALGORITHM);
        //byte[] iv = new byte[IV_LENGTH];
        //random.nextBytes(iv);
        byte[] iv = initialVectorString.getBytes();
        Log.d(TAG, "initialVectorString "+ initialVectorString);
        return iv;
    }

    public static String randomString(char[] characterSet, int length) {
        SecureRandom random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        String str = new String(result);
        try{
            str = new String(str.getBytes(),"UTF8");
        }
        catch(Exception e){

        }
        Log.d("check", str.getBytes().length + "");
        return str;
    }

//    public static void main(String[] args) {
//        String key = "1234567891234567";
//        String data = "example";
//        System.out.println(Security.decrypt(Security.encrypt(data, key), key));
//        System.out.println(Security.encrypt(data, key));
//    }

    private byte[] toBytes(char[] chars) {
        CharBuffer charBuffer = CharBuffer.wrap(chars);
        ByteBuffer byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
        byte[] bytes = Arrays.copyOfRange(byteBuffer.array(),
                byteBuffer.position(), byteBuffer.limit());
        Arrays.fill(charBuffer.array(), '\u0000'); // clear sensitive data
        Arrays.fill(byteBuffer.array(), (byte) 0); // clear sensitive data
        return bytes;
    }

}
