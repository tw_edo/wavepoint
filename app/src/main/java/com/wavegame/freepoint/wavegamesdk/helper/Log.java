package com.wavegame.freepoint.wavegamesdk.helper;

/**
 * Created by wavegame9 on 2/29/2016.
 */
public class Log {
    public static final boolean ENABLE_LOG = true;
    public static final String CUSTOM_PREFIX = "faruk =>";

    public static void d(String tag, String logMessage){
        if(ENABLE_LOG){
            android.util.Log.d(tag, CUSTOM_PREFIX+logMessage);
        }
    }

    public static void v(String tag, String logMessage){
        if(ENABLE_LOG){
            android.util.Log.v(tag, CUSTOM_PREFIX + logMessage);
        }
    }

    public static void e(String tag, String logMessage){
        if(ENABLE_LOG){
            android.util.Log.e(tag, CUSTOM_PREFIX + logMessage);
        }
    }

    public static void i(String tag, String logMessage){
        if(ENABLE_LOG){
            android.util.Log.i(tag, CUSTOM_PREFIX + logMessage);
        }
    }
}
