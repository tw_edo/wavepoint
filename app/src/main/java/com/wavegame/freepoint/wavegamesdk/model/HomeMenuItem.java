package com.wavegame.freepoint.wavegamesdk.model;

import java.io.Serializable;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class HomeMenuItem implements Serializable {

    private String menuName;
    private String picFileName;
    private String activityString;
    private int colorCircle;


    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getPicFileName() {
        return picFileName;
    }

    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

    public String getActivityString() {
        return activityString;
    }

    public void setActivityString(String activityString) {
        this.activityString = activityString;
    }

    public int getColorCircle() {
        return colorCircle;
    }

    public void setColorCircle(int colorCircle) {
        this.colorCircle = colorCircle;
    }
}
