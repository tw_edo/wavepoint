package com.wavegame.freepoint.wavegamesdk.rahasia;

import android.os.Build;
import android.util.Base64;

import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
//import javax.xml.bind.DatatypeConverter;

public class PublicKeyDecrypt {

    private static final String TAG = PublicKeyDecrypt.class.getSimpleName();

    public static String show(String encrypted) {
        //String encrypted = "t8s4KdNUwQubM6Du4aOIoO+NUTI8YxWq/lMXzVcd89g5CSqZtEkjgVFH6bqgFqi4znGRzPTpnU2GcouEbAoyyHmGpURH+NGbD/cD97HzvkvF4BXXM871g2Iz5xdkG5P+orojTt+N05QNCLlwBUcRS5r7btlanOHYXjVV4x6nZOpIzAPaMjmlmNDw0KRKSk1eIYivWY9B9NNK4oNrRjFBZlTTfW95+UkER8TgPiIl3gTIKXJRXpsX/1SSMduKCfKNWYOrOwkV5xbZvoeQspTwznuHQWAr57nHtCWZSq7QJgVNnZGDZadITdEQpYCVf7IM3ppDo7cIL89pAzcG50UPlw==";
//        String encrypted = "Ujle0JAwgVNEXU9tZwiE9b8Gz5FYGs-qkvpv6xCQagb__v1CdDHHpArQGlmwiPYS7_WPfBCPwi85oLd-b4RzgnnTRMcO0LAEMgRuc6UblZGt7o1JV_Tip2UyDVciD4EuDC6fVCvd_q8paD8EvtuclSKzX1e_AIk1nP3mU_y-Gv4EYOkPOumQqhCtP8mhtZCLy815FbWhiaxxpagA4mKHjOJ6FDkuKCEidqzsTJ5mGsQAb8Zimyyymdaj6w-qlLHa6sGsRxXWKey9dWHzVScuP0ygAyG1YgYS83ZcuK7pwxiphnIvJnuAjcV9kfU4poFWHUsGI2VtwXNkOf_vSsoHmQ==";

//        String encrypted = "PpFCxF2F59yt4DqVy2fyCZFcuRdHJ1yPmQF99neZwW6HOlC9tF5cEd7OwCQurYu8SVdslJzTqr+2UJ2UtRvxS728TF0CfJb32Tn7MQ9K/KTu1eEzvJCPIBltxEEOhl+lkOjgDuPrM+n+PuKTSoWvfnHX//OMNUtFhcZ+SOW/pFXAsyLLnQdjIfqAw5FC+bTpqCUg1Z4gk92+ghLM8+pF1VnFtOW6SwnwQYwHQUp8nKI3JBe1v1UgoHl/Pm74pIIyIA6pgJJ9VZNSB6LXaq1O+uHB5deEoBhaG21D8BSAdYd4uqWDnDvGH65dVq970EmL8nFLgTigILDsKRFGLBFLnw==";
        String publicKey  = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0Fxfe4IB6k8NPSxKxZFe\n";
               publicKey += "t5apGAVEDt/okSB8dPZ2s8gfX0zJJsyVl9TPWAkzrFJEUj6FxA014ch7DQykarHc\n";
               publicKey += "Pb35d7ct7ak5ct1Y52Wrzkvo8N0uLPWJBt6wxPOfhPUp5oTqija62Zylalqz8cjZ\n";
               publicKey += "w9n2PtNoKSOdcogFIs9KTrVUP4v6R2rbbT+8DYhKfUADMOENNyzbB/Y8+W9PMhAt\n";
               publicKey += "XAxmYmxaDJXGkLiI/yU+nuKbPQBLJOmK9gxTiIiMvVbGgfVCmPUONgMcCfSHzL6J\n";
               publicKey += "gVGmEXE7AgFt/curkPcULnV9ab5UO0dfJJCeLXz56TAUkOQqa2Z+3eSDPezPZiwZ\n";
               publicKey += "1QIDAQAB\n";

//        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDW2x6kbdpLOMY4l+raQobW5tpY\n" +
//                "AtGhajlOYPtNBDRuaSe8vf3ae22Yj7KxPNRuovvN5snfqkrjavMnecgJeoTCiyR3\n" +
//                "KLEDFCnQNZc2Y4CyKVUbsmcIdEnzxFWkKLj1SvUjpndz6DGmFprEtA32kuBlk3GE\n" +
//                "kdu1Nf8N6DbPZcNO7QIDAQAB";

        byte[] encryptedBytes = Base64.decode(encrypted, Constants.HTTP.BASE64_MODE);
        byte[] keyBytes = Base64.decode(publicKey, Constants.HTTP.BASE64_DEFAULT);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        String plainText = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey key = keyFactory.generatePublic(spec);
            byte[] decryptedText = null;
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decryptedText = cipher.doFinal(encryptedBytes);
            plainText = new String(decryptedText, "UTF-8");

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        Log.d(TAG, "Decrypted text Public: " + plainText);

        return plainText;
    }

}