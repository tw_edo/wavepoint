package com.wavegame.freepoint.wavegamesdk.rahasia;

import android.util.Base64;

import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
//import javax.xml.bind.DatatypeConverter;

public class PublicKeyEncrypt {

    private static final String TAG = PublicKeyEncrypt.class.getSimpleName();

    public static String show(String plainText) {
//        String plainText = "w05J+i+RQKa3iR8cTSCwUDKBdq0kIhPEeoZ06qD8yxg=";


        String publicKey  = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0Fxfe4IB6k8NPSxKxZFe\n";
        publicKey += "t5apGAVEDt/okSB8dPZ2s8gfX0zJJsyVl9TPWAkzrFJEUj6FxA014ch7DQykarHc\n";
        publicKey += "Pb35d7ct7ak5ct1Y52Wrzkvo8N0uLPWJBt6wxPOfhPUp5oTqija62Zylalqz8cjZ\n";
        publicKey += "w9n2PtNoKSOdcogFIs9KTrVUP4v6R2rbbT+8DYhKfUADMOENNyzbB/Y8+W9PMhAt\n";
        publicKey += "XAxmYmxaDJXGkLiI/yU+nuKbPQBLJOmK9gxTiIiMvVbGgfVCmPUONgMcCfSHzL6J\n";
        publicKey += "gVGmEXE7AgFt/curkPcULnV9ab5UO0dfJJCeLXz56TAUkOQqa2Z+3eSDPezPZiwZ\n";
        publicKey += "1QIDAQAB\n";

        Log.d(TAG, publicKey);
        Log.d(TAG, "input = "+plainText);

//        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDW2x6kbdpLOMY4l+raQobW5tpY\n" +
//                "AtGhajlOYPtNBDRuaSe8vf3ae22Yj7KxPNRuovvN5snfqkrjavMnecgJeoTCiyR3\n" +
//                "KLEDFCnQNZc2Y4CyKVUbsmcIdEnzxFWkKLj1SvUjpndz6DGmFprEtA32kuBlk3GE\n" +
//                "kdu1Nf8N6DbPZcNO7QIDAQAB";

        byte[] plainTextBytes = plainText.getBytes();
        byte[] keyBytes = Base64.decode(publicKey, Constants.HTTP.BASE64_DEFAULT);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        String encryptedEncoded = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey key = keyFactory.generatePublic(spec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encrypted = cipher.doFinal(padString(plainText).getBytes());
//            encryptedEncoded = DatatypeConverter.printBase64Binary(encrypted);
            encryptedEncoded = new String(Base64.encode(encrypted, Constants.HTTP.BASE64_MODE),"UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        try {
            Log.d(TAG, "Encrypted AES with Public Key: " + URLEncoder.encode(encryptedEncoded,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encryptedEncoded;
    }

    private static String padString(String source)
    {
        char paddingChar = 0;
        int size = 16;
        int x = source.length() % size;
        int padLength = size - x;

        for (int i = 0; i < padLength; i++)
        {
            source += paddingChar;
        }

        return source;
    }

    public static String encrypt(String plainTextAES) {


        String  publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0Fxfe4IB6k8NPSxKxZFe\n";
        publicKey += "t5apGAVEDt/okSB8dPZ2s8gfX0zJJsyVl9TPWAkzrFJEUj6FxA014ch7DQykarHc\n";
        publicKey += "Pb35d7ct7ak5ct1Y52Wrzkvo8N0uLPWJBt6wxPOfhPUp5oTqija62Zylalqz8cjZ\n";
        publicKey += "w9n2PtNoKSOdcogFIs9KTrVUP4v6R2rbbT+8DYhKfUADMOENNyzbB/Y8+W9PMhAt\n";
        publicKey += "XAxmYmxaDJXGkLiI/yU+nuKbPQBLJOmK9gxTiIiMvVbGgfVCmPUONgMcCfSHzL6J\n";
        publicKey += "gVGmEXE7AgFt/curkPcULnV9ab5UO0dfJJCeLXz56TAUkOQqa2Z+3eSDPezPZiwZ\n";
        publicKey += "1QIDAQAB\n";

        Log.d(TAG, "input = " + plainTextAES);


        byte[] plainTextBytes = plainTextAES.getBytes();
        byte[] keyBytes = Base64.decode(publicKey,Base64.NO_PADDING);

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        String encryptedEncoded = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey key = keyFactory.generatePublic(spec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] encrypted = cipher.doFinal(plainTextBytes);
//            encryptedEncoded = DatatypeConverter.printBase64Binary(encrypted);
            encryptedEncoded = new String(Base64.encode(encrypted, Base64.NO_PADDING),"UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        try {
            Log.d(TAG, "Base64 Encode AES with Public Key: " + encryptedEncoded);
            Log.d(TAG, "URL Encrypted AES with Public Key: " + URLEncoder.encode(encryptedEncoded,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encryptedEncoded;
    }
}