package com.wavegame.freepoint.wavegamesdk.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @author Muhammad Faruk A. Rahman
 * @version 0.0.0
 * @date 2/22/2016
 */
public class AppItem implements Serializable {

    //application unique id
    @Expose
    private String appId;

    //id untuk advertiser, 1 app bisa di-advertise oleh advertiser yang berbeda
    @Expose
    private String advertiserId;

    //application name
    @Expose
    private String appName;

    //package com.blablabla
    @Expose
    private String appPackage;

    //url for picture
    @Expose
    private String appPicUrl;

    //url for icon picture
    @Expose
    private String appIconPicUrl;

    //application description
    @Expose
    private String appDescription;

    @Expose
    private double installReward;

    //indicator if user already paid for installation reward
    @Expose
    private int isInstallRewardDone;

    /*
    for daily Play
     */
    @Expose
    private double dailyPlayReward;

    @Expose
    private int currentDailyPlay;

    @Expose
    private int maxDailyPlay;

    @Expose
    private int requireMinuteDailyPlay;

    /*
    for completing daily play
     */
    @Expose
    private double dailyPlayCompleteReward;

    /*
    for not uninstall
     */

    @Expose
    private double notUninReward;

    @Expose
    private int currentNotUnin;

    @Expose
    private int maxNotUnin;

    //indicate if reward for not uninstall for certain time is already paid
    @Expose
    private int isNotUninRewardDone;

    /*
    *
    *   GETTER AND SETTER METHOD'S
     */


    public int getMaxDailyPlay() {
        return maxDailyPlay;
    }

    public void setMaxDailyPlay(int maxDailyPlay) {
        this.maxDailyPlay = maxDailyPlay;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    public String getAppPicUrl() {
        return appPicUrl;
    }

    public void setAppPicUrl(String appPicUrl) {
        this.appPicUrl = appPicUrl;
    }

    public String getAppIconPicUrl() {
        return appIconPicUrl;
    }

    public void setAppIconPicUrl(String appIconPicUrl) {
        this.appIconPicUrl = appIconPicUrl;
    }

    public String getAppDescription() {
        return appDescription;
    }

    public void setAppDescription(String appDescription) {
        this.appDescription = appDescription;
    }

    public double getInstallReward() {
        return installReward;
    }

    public void setInstallReward(double installReward) {
        this.installReward = installReward;
    }

    public int getIsInstallRewardDone() {
        return isInstallRewardDone;
    }

    public void setIsInstallRewardDone(int isInstallRewardDone) {
        this.isInstallRewardDone = isInstallRewardDone;
    }

    public double getDailyPlayReward() {
        return dailyPlayReward;
    }

    public void setDailyPlayReward(double dailyPlayReward) {
        this.dailyPlayReward = dailyPlayReward;
    }

    public int getCurrentDailyPlay() {
        return currentDailyPlay;
    }

    public void setCurrentDailyPlay(int currentDailyPlay) {
        this.currentDailyPlay = currentDailyPlay;
    }

    public int getRequireMinuteDailyPlay() {
        return requireMinuteDailyPlay;
    }

    public void setRequireMinuteDailyPlay(int requireMinuteDailyPlay) {
        this.requireMinuteDailyPlay = requireMinuteDailyPlay;
    }

    public double getDailyPlayCompleteReward() {
        return dailyPlayCompleteReward;
    }

    public void setDailyPlayCompleteReward(double dailyPlayCompleteReward) {
        this.dailyPlayCompleteReward = dailyPlayCompleteReward;
    }

    public double getNotUninReward() {
        return notUninReward;
    }

    public void setNotUninReward(double notUninReward) {
        this.notUninReward = notUninReward;
    }

    public int getCurrentNotUnin() {
        return currentNotUnin;
    }

    public void setCurrentNotUnin(int currentNotUnin) {
        this.currentNotUnin = currentNotUnin;
    }

    public int getMaxNotUnin() {
        return maxNotUnin;
    }

    public void setMaxNotUnin(int maxCurrentNotUnin) {
        this.maxNotUnin = maxCurrentNotUnin;
    }

    public int getIsNotUninRewardDone() {
        return isNotUninRewardDone;
    }

    public void setIsNotUninRewardDone(int isNotUninRewardDone) {
        this.isNotUninRewardDone = isNotUninRewardDone;
    }
}
