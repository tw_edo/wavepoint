package com.wavegame.freepoint.wavegamesdk.rahasia;

import android.util.Base64;

import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.crypto.Cipher;
//import javax.xml.bind.DatatypeConverter;

public class PrivateKeyDecrypt {

    private static final String TAG = PrivateKeyDecrypt.class.getSimpleName();

    public static String show(String encrypted) {
//        String encrypted = "CW2moAFPEMTitEhba5LWWYwm8HVwWwLnQ/9SbqR7QAAMhxqOEhzq1UxcDcgUiCxRPmeFGviQstlU5z4TK0r2HV9tVYIEs0H1tdsZBclxoD9mChx/8agWjy3ihebhOK6YoOqciQ1ZA4RP6jIERrThql8g18N5juoYAN3iniA8pwmSh0LKMSkbi8g8tsBqysWzAKmwy5cK48vt+TkY10sM6J3vrEhGy96FgdEnQjQ0rIv3sdRBOltV+9fMM79ysAIYHwOREF3ESyPtxbY9gZqWwucIbobVPywRcvOq5yMi29RRlXNnYTPtEj038duzLjcQUplabaZrtIK6/TeBIQCdfg==";
//        String encrypted = "JMOWVQcKPCPrwfkzRM/oDHGa/AZ4tyq0+6Cmn8YCtlQSnvLM8oan6TBuTDWPefB/fsBuR5V7DIav4X7pnt+TfU6UyWQVYfKi9698buc4p7vk6N59UCyGe4F8L9lEjHRAoWQVqLfL4WYwV1GUh5c8Ecrr0n5fQnpkHFRe0VRJlruEHzz2z9LAt44KqpOq9f/WmYVRQHEBKb8WHTs7aEoSzPcBwHx7aGf4bkAYaanwpzSxy1i3gZDzpxy7Oc/guo/lEuyrejbGBugdFcL+u4TU9KMTtUIuHNZo240YjJRO5mBamXes6XxiIcd1y6fEIrZDCuf8rt1gYdd3hdBlbgu8sg==";

        String privateKey  = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDQXF97ggHqTw09\n";
               privateKey += "LErFkV63lqkYBUQO3+iRIHx09nazyB9fTMkmzJWX1M9YCTOsUkRSPoXEDTXhyHsN\n";
               privateKey += "DKRqsdw9vfl3ty3tqTly3VjnZavOS+jw3S4s9YkG3rDE85+E9SnmhOqKNrrZnKVq\n";
               privateKey += "WrPxyNnD2fY+02gpI51yiAUiz0pOtVQ/i/pHatttP7wNiEp9QAMw4Q03LNsH9jz5\n";
               privateKey += "b08yEC1cDGZibFoMlcaQuIj/JT6e4ps9AEsk6Yr2DFOIiIy9VsaB9UKY9Q42AxwJ\n";
               privateKey += "9IfMvomBUaYRcTsCAW39y6uQ9xQudX1pvlQ7R18kkJ4tfPnpMBSQ5CprZn7d5IM9\n";
               privateKey += "7M9mLBnVAgMBAAECggEBAKLhk0LZvuWMJq1LkqgfnAuom0dL1ohKQWQyIRjOEr04\n";
               privateKey += "FE5LmHPcOpUWp4AKsXpkVUR129wG97gpIQhyeZVutN0PRZVwfVbskxxYLMe3kzcR\n";
               privateKey += "4GdqgE5lI1lbfJR6Xplxje/GuBKT9j1nV2hsv36jpJiWKwmx+hsu3EZMTreHlG8x\n";
               privateKey += "qBqiXkzrY/DtQKtCh14C1QYbof12y4DdmXM8V/bHiGOzX52VxNuDmIv9IwuevdXf\n";
               privateKey += "6eAOdwkwqEihcfzjvDF0gjRbBc9IEFKMKLp6pTE28FC74qafLTOMogDxMDZGmWP1\n";
               privateKey += "thN8rS3UCbn3IFtmNx5+pEKA7CPqNXowYDT8QNXVwYECgYEA7EeD46AjE8qVXHgW\n";
               privateKey += "GLiqvPepy+he26nQwW2r1K34qOMO9axdlpJO99djx4M6xfQurD7HHzYNNycVy9o/\n";
               privateKey += "lm+a15DrwoM90Wt9zQasBn94tB0WWLMxyqlu2lszp/aq49r16gjIUanjlvpyZSmz\n";
               privateKey += "Jsk9MCw5Rs07JyrTUo3ag8IH2mMCgYEA4cBVg/LdXK5Al20ioYkMn+H8sPPbi1Jt\n";
               privateKey += "oD0kzT+oOy6oW1wWREVOs8hpyZhegvURUQMPAKRPDF3dc9yMJ5tcsdDAS5YL05qR\n";
               privateKey += "fBJMBkeJJNLST5gfXBrhx4jp9QO3uSXZ8vP5rzUfESs7rDXN47saX6ujJAiNExST\n";
               privateKey += "slcGWpTWlGcCgYBtQvzobAsMgefj5NxC/lJmJBju5DvJVJPmfrKxHOoaHzWxwc1d\n";
               privateKey += "bbb6wS2w+yxYkfxrRnSKKFKgKIDZzckN5foyqR98yXfyucRo5Sbzskq8755/0g26\n";
               privateKey += "a/OJIheXGmgYCg8h9IG90dQe4x23Tjs8Ol8DQdASv6EVEaa7TEG6ICXFhQKBgQDZ\n";
               privateKey += "8FleUgdcYykeRxDoy6sxavUNS87GWORWjtDJiUv7HpcP4/3nGNXbAH8ITR3o5Xg7\n";
               privateKey += "Mm24W+eoc7HIGcIAtp8tVu9dyVRPi2gqVGuqoEMJFrJMfvq/a4BOpqHelSE4EA+x\n";
               privateKey += "2dFRUUJVy9wG68aBtO7IL6YGsm6nXfUcdaE/r3LcvwKBgQCsUl7SJue6d71Zlf2/\n";
               privateKey += "o1zXPzAaOKRXQ5kjgpSGnC/6E6HgYcn3QDiYYVUFIzTI8TNsVMts8nyyoQa7Gt2s\n";
               privateKey += "hdHtckzsKnHKWY0kj6PcXm0v+dsqezEc3UFGDsJL4Hv7mF6LiU6swNulIvdFEV3E\n";
               privateKey += "cul+KaJ/Y02uD06J0H5eZmyJcQ==\n";

//        String privateKey = "MIICXAIBAAKBgQDW2x6kbdpLOMY4l+raQobW5tpYAtGhajlOYPtNBDRuaSe8vf3a\n" +
//                "e22Yj7KxPNRuovvN5snfqkrjavMnecgJeoTCiyR3KLEDFCnQNZc2Y4CyKVUbsmcI\n" +
//                "dEnzxFWkKLj1SvUjpndz6DGmFprEtA32kuBlk3GEkdu1Nf8N6DbPZcNO7QIDAQAB\n" +
//                "AoGASi718YgMaxR7PGcXq6x0qhajJOg0QStQBgPuQLVG9ShRlBV1ZM5hFN/ortF1\n" +
//                "941Gq90XrqY5nTP8KJEfxEYNOAwIi3k47OglMpm8b2a13IbQ+LZzSUbOpki0vU+e\n" +
//                "pB7VmjcLJJFNvzumhxLPcV6HY4Z0HgWJ29cPLsDdnvR+QgECQQD/7sU4VJorRSL+\n" +
//                "pOfOivpjcCBjvkkPDolowmU7ZLb29isperK9K0RHJCbEdOcZbVWlg5/SKPBQZbv3\n" +
//                "3iXcpzINAkEA1umVf+auURayNGDCH8inYi8FobMdIymeUPOdAWCCmWVG/vxaB6fX\n" +
//                "UYCh5zKPEURWUsIsUka7p5zIuqvXLDC4YQJBAOxKK2XdnlUSyMNnxukHoTMLg6vz\n" +
//                "NAqltjG1QLd8Pfx2vNTYBovmlWxFMVGySOdN7YQZqQXkbDMeKlQnkrdM2kUCQFcB\n" +
//                "Ucjp9wBVE+gok+6SKVGSf9Eq1kz5+GIFfjakf7riSY/JUhlvVbhlPyZ+TXVMGTZ/\n" +
//                "7bmRcT82Nf18vCvjvYECQHoDSF2nEZlEYwegjttROiB/R9B+DP0QTmkCyyD8fmeL\n" +
//                "50Miyt9JuVv2wxtmxUKLWcqwKtgyrGWA40Ba9OxXyKQ=";

//        byte[] encryptedBytes = DatatypeConverter.parseBase64Binary(encrypted);

        //Log.d(TAG, privateKey);

        byte[] encryptedBytes = Base64.decode(encrypted, Constants.HTTP.BASE64_MODE);
//        byte[] keyBytes = DatatypeConverter.parseBase64Binary(privateKey);
        byte[] keyBytes = Base64.decode(privateKey, Constants.HTTP.BASE64_DEFAULT);

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        String plainText = null;

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey key = keyFactory.generatePrivate(spec);
            byte[] decryptedText = null;
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decryptedText = cipher.doFinal(encryptedBytes);
            plainText = new String(decryptedText, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        Log.d(TAG, "Decrypted text: " + plainText);
        return plainText;

    }

}