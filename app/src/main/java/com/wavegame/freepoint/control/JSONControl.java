package com.wavegame.freepoint.control;

import android.util.Log;

import com.google.gson.Gson;
import com.wavegame.freepoint.utilities.ConfigManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONControl {
    private JSONResponse _JSONResponse;


    public JSONControl() {
        _JSONResponse = new JSONResponse();
    }


    public JSONObject postRegister(String DATAUSR, String AES, String IV) {

        JSONObject jsonObj = null;

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("a", DATAUSR));
            params.add(new BasicNameValuePair("b", AES));
            params.add(new BasicNameValuePair("c", IV));
            jsonObj = _JSONResponse.POSTResponse(ConfigManager.REGISTER, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    public JSONObject postLoginWP(String a, String b, String c) {

        JSONObject jsonObj = null;

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("a", a));
            params.add(new BasicNameValuePair("b", b));
            params.add(new BasicNameValuePair("c", c));
            jsonObj = _JSONResponse.POSTResponse(ConfigManager.LOGIN_WP, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }


    public JSONObject postLoginWG(String a, String b, String c) {

        JSONObject jsonObj = null;

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("a", a));
            params.add(new BasicNameValuePair("b", b));
            params.add(new BasicNameValuePair("c", c));
            jsonObj = _JSONResponse.POSTResponse(ConfigManager.LOGIN_WG, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public JSONObject postViewProfile(String a, String b, String c) {

        JSONObject jsonObj = null;

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("a", a));
            params.add(new BasicNameValuePair("b", b));
            params.add(new BasicNameValuePair("c", c));
            jsonObj = _JSONResponse.POSTResponse(ConfigManager.VIEW_PROFILE, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public JSONObject postEditProfile(String a, String b, String c) {

        JSONObject jsonObj = null;

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("a", a));
            params.add(new BasicNameValuePair("b", b));
            params.add(new BasicNameValuePair("c", c));
            jsonObj = _JSONResponse.POSTResponse(ConfigManager.EDIT_PROFILE, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    public JSONObject postViewBalance(String a, String b, String c) {

        JSONObject jsonObj = null;

        try {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("a", a));
            params.add(new BasicNameValuePair("b", b));
            params.add(new BasicNameValuePair("c", c));
            jsonObj = _JSONResponse.POSTResponse(ConfigManager.CEK_BALANCE, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

}
