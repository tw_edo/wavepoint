package com.wavegame.freepoint.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.wavegame.freepoint.model.ModelUser;


/**
 * Created by User on 10/21/2015.
 */
public class ApplicationManager {
    private static ApplicationManager sInstance = null;

    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    private static final int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "WavePoinApplication";

    private static final String KEY_REFRESH_TOKEN = "refreshToken";
    private static final String KEY_EXPIRED_ON = "expiredOn";
    private static final String KEY_USER_EMAIL = "userEmail";
    private static final String KEY_TOKEN1 = "token1";
    private static final String KEY_TOKEN2 = "token2";
    private static final String KEY_TOKEN3 = "token3";
    private static final String KEY_USER = "user";
    private static final String KEY_FREEPOIN = "freepoin";
    private Context mContext;


    public ApplicationManager(Context context){
        mContext = context;
        mPref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mPref.edit();
    }

    public static ApplicationManager getInstance(Context ctx) {
        if (sInstance == null)
            sInstance = new ApplicationManager(ctx);
        return sInstance;
    }

    public void setUser(ModelUser user) {
        try {
            Gson gson = new Gson();
            String json = gson.toJson(user);
            mEditor.putString(KEY_USER, json);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
        mEditor.commit();
    }
    public ModelUser getUser() {
        Gson gson = new Gson();
        String json = mPref.getString(KEY_USER, null);
        ModelUser user = gson.fromJson(json,ModelUser.class);
        return user;
    }

    public void setUserToken1(String userToken) {
        try {
            mEditor.putString(KEY_TOKEN1, userToken);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
        mEditor.commit();
    }
    public String getUserToken1() {
        return mPref.getString(KEY_TOKEN1, null);
    }

    public void setUserToken2(String userToken) {
        try {
            mEditor.putString(KEY_TOKEN2, userToken);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
        mEditor.commit();
    }
    public String getUserToken2() {
        return mPref.getString(KEY_TOKEN2, null);
    }

    public void setUserToken3(String userToken) {
        try {
            mEditor.putString(KEY_TOKEN3, userToken);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
        mEditor.commit();
    }
    public String getUserToken3() {
        return mPref.getString(KEY_TOKEN3, null);
    }

    public void setFreepoin(String freepoin) {
        try {
            mEditor.putString(KEY_FREEPOIN, freepoin);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
        mEditor.commit();
    }

    public String getFreepoin() {
        return mPref.getString(KEY_FREEPOIN, null);
    }

    public void setUserMail(String userMail) {
        try {
            mEditor.putString(KEY_USER_EMAIL, userMail);
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullPointerException();
        }
        mEditor.commit();
    }

    public String getUserMail() {
        return mPref.getString(KEY_USER_EMAIL, null);
    }

    public void logoutUser() {
        mEditor.clear();
        mEditor.commit();
    }




}
