package com.wavegame.freepoint.utilities;

public class ConfigManager {
    public static final String SERVER = "http://wp-alpha-56.wavedev.net/rest_accounts/";
    public static final String REGISTER = SERVER + "add.json";
    public static final String LOGIN_WP = SERVER + "login.json";
    public static final String LOGIN_WG = SERVER + "addwg.json";
    public static final String VIEW_PROFILE = SERVER + "getProfile.json";
    public static final String EDIT_PROFILE = SERVER + "editProfile.json";
    public static final String CEK_BALANCE = SERVER + "getBalance.json";
}
