package com.wavegame.freepoint.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.adapter.RecyclerAdapter;
import com.wavegame.freepoint.ui.adapter.RecyclerInviteAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class InviteFragment extends Fragment {

    private static final String TAG = InviteFragment.class.getSimpleName();
    public static final String ITEMS_COUNT_KEY = "DummyListFragment$ItemsCount";

    public static InviteFragment createInstance(int itemsCount) {
        InviteFragment dummyFragment = new InviteFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ITEMS_COUNT_KEY, itemsCount);
        dummyFragment.setArguments(bundle);
        return dummyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_dummy, container, false);
        setupRecyclerView(recyclerView);
        return recyclerView;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerInviteAdapter recyclerInviteAdapter = new RecyclerInviteAdapter(createItemList());
        recyclerView.setAdapter(recyclerInviteAdapter);
    }

    private List<String> createItemList() {
        List<String> itemList = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            int itemsCount = bundle.getInt(ITEMS_COUNT_KEY);
            for (int i = 0; i < itemsCount; i++) {
                switch (i%5) {
                    case 0:
                        itemList.add("Facebook");
                        break;
                    case 1:
                        itemList.add("Twitter");
                        break;
                    case 2:
                        itemList.add("Yahoo Mail");
                        break;
                    case 3:
                        itemList.add("Line");
                        break;
                    case 4:
                        itemList.add("SMS");
                        break;
                }
            }
        }
        return itemList;
    }


}
