package com.wavegame.freepoint.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class InviteItemHolder extends RecyclerView.ViewHolder {
    private ImageView iv_icon_invite;
    private TextView tv_invite_via;
    private Button btn_send_invit;

    public InviteItemHolder(View itemView) {
        super(itemView);
        iv_icon_invite = (ImageView) itemView.findViewById(R.id.iv_icon_invite);
        tv_invite_via = (TextView) itemView.findViewById(R.id.tv_invite_via);
        btn_send_invit = (Button) itemView.findViewById(R.id.btn_send_invit);
    }

    public ImageView getIv_icon_invite() {
        return iv_icon_invite;
    }

    public TextView getTv_invite_via() {
        return tv_invite_via;
    }

    public Button getBtn_send_invit() {
        return btn_send_invit;
    }
}
