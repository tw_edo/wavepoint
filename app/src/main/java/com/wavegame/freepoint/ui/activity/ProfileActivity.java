package com.wavegame.freepoint.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.control.JSONControl;
import com.wavegame.freepoint.model.ModelUser;
import com.wavegame.freepoint.utilities.ApplicationManager;
import com.wavegame.freepoint.utilities.DialogManager;
import com.wavegame.freepoint.wavegamesdk.rahasia.MCrypt;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Arrays;

/**
 * Created by wavegame9 on 3/1/2016.
 */
public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = ProfileActivity.class.getSimpleName();
    private CallbackManager callbackManager;
    private Button btnLoginEmail, btnConfirm;
    private TextView btnRegister, txtNamaAwal, txtNamaAkhir;
    private Context mContext;
    private ApplicationManager applicationManager;
    private Activity mActivity;
    private EditText txtAlamat, txtKota, txtKodePos, txtTelepon, txtHP, txtGender, txtTanggalLahir;
    private String strUserId, strToken1, strToken2, strToken3, strNamaAwal, strNamaAkhir, strAlamat, strKota, strKodePos, strTelepon, strHP, strGender, strTanggalLahir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_profile);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mActivity = this;
        applicationManager = new ApplicationManager(mActivity);
        txtNamaAwal = (TextView) findViewById(R.id.txtNamaAwal);
        txtNamaAkhir = (TextView) findViewById(R.id.txtNamaAkhir);
        txtAlamat = (EditText) findViewById(R.id.txtAlamat);
        txtKota = (EditText) findViewById(R.id.txtKota);
        txtKodePos = (EditText) findViewById(R.id.txtKodePos);
        txtTelepon = (EditText) findViewById(R.id.txtTelepon);
        txtHP = (EditText) findViewById(R.id.txtHP);
        txtGender = (EditText) findViewById(R.id.txtGender);
        txtTanggalLahir = (EditText) findViewById(R.id.txtTanggalLahir);
        btnConfirm = (Button) findViewById(R.id.btnConfirm);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strUserId = applicationManager.getUser().getId();
                strToken1 = applicationManager.getUserToken1();
                strToken2 = applicationManager.getUserToken2();
                strToken3 = applicationManager.getUserToken3();
                strNamaAwal = txtNamaAwal.getText().toString();
                strNamaAkhir = txtNamaAkhir.getText().toString();
                strAlamat = txtAlamat.getText().toString();
                strKota = txtKota.getText().toString();
                strKodePos = txtKodePos.getText().toString();
                strTelepon = txtTelepon.getText().toString();
                strHP = txtHP.getText().toString();
                strGender = txtGender.getText().toString();
                strTanggalLahir = txtTanggalLahir.getText().toString();
                String input_dataUsr = "{\"datausrid\":\"" + strUserId +
                        "\",\"datatoken1\":\"" + strToken1 +
                        "\",\"datatoken2\":\"" + strToken2 +
                        "\",\"datatoken3\":\"" + strToken3 +
                        "\",\"datanmawal\":\"" + strNamaAwal +
                        "\",\"datanmakhir\":\"" + strNamaAkhir +
                        "\",\"dataalamat\":\"" + strAlamat +
                        "\",\"datakota\":\"" + strKota +
                        "\",\"datakp\":\"" + strKodePos +
                        "\",\"datatelp\":\"" + strTelepon +
                        "\",\"datahp\":\"" + strHP +
                        "\",\"datagender\":\"" + strGender +
                        "\",\"dataultah\":\"" + strTanggalLahir +
                        "\",\"datafoto\":\"" + "-" + "\"" +
                        "}";

                com.wavegame.freepoint.wavegamesdk.helper.Log.d(TAG, "input_dataUsr = " + input_dataUsr);

                try {
                    MCrypt mcrypt = new MCrypt();
                    String a = URLEncoder.encode(new String(Base64.encode(mcrypt.encrypt(input_dataUsr), Base64.DEFAULT)), "UTF-8");
                    String b = URLEncoder.encode(new String(Base64.encode(mcrypt.getEncryptedAES(), Base64.DEFAULT)), "UTF-8");
                    String c = URLEncoder.encode(new String(Base64.encode(mcrypt.getIv().getBytes(), Base64.DEFAULT)), "UTF-8");
                    Log.d("EDO", "a = " + a);
                    Log.d("EDO", "b = " + b);
                    Log.d("EDO", "c = " + c);
                    new DoPostProfile(mActivity).execute(a, b, c);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProfileActivity.this, FreepointMainActivity.class);
        startActivity(intent);
        finish();
    }


    private class DoPostProfile extends AsyncTask<String, Void, String> {
        private String message = "Tidak dapat edit profile";
        private Activity activity;
        private Context context;
        private Resources resources;
        private ProgressDialog progressDialog;
        String error = "";

        public DoPostProfile(Activity activity) {
            super();
            this.activity = activity;
            this.context = activity.getApplicationContext();
            this.resources = activity.getResources();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Saving. . .");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String a = params[0];
                String b = params[1];
                String c = params[2];

                JSONControl jsControl = new JSONControl();
                JSONObject responseRegister = jsControl.postEditProfile(a, b, c);
                Log.d("json responseRegister", responseRegister.toString());
                ModelUser user = new ModelUser();
                String _id = responseRegister.getString("accid");
                String _token1 = responseRegister.getString("token1");
                String _token2 = responseRegister.getString("token2");
                String _token3 = responseRegister.getString("token3");
                String stat = responseRegister.getString("stat");
                user.setId(_id);
                user.setNama(responseRegister.getString("NMAWAL ") + responseRegister.getString("NMAKHIR"));
                ApplicationManager.getInstance(activity).setUser(user);
                ApplicationManager.getInstance(activity).setUserToken1(_token1);
                ApplicationManager.getInstance(activity).setUserToken2(_token2);
                ApplicationManager.getInstance(activity).setUserToken3(_token3);
                message = stat;
                if(!stat.contains("Sukses")){
                    return "FAIL";
                }
                else {
                    return "OK";
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return "FAIL";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();
            switch (result) {
                case "FAIL":
                    DialogManager.showDialog(mActivity, "Mohon Maaf", message);
                    break;

                case "OK":
                    Intent i = new Intent(getBaseContext(), FreepointMainActivity.class);
                    startActivity(i);
                    finish();
                    break;
            }
        }


    }


}
