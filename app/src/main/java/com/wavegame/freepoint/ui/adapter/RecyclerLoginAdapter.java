package com.wavegame.freepoint.ui.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;
import com.supersonicads.sdk.utils.DeviceProperties;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.activity.FreepointMainActivity;
import com.wavegame.freepoint.ui.viewholder.LoginItemHolder;
import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;
import com.wavegame.freepoint.wavegamesdk.rahasia.PrivateKeyDecrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.PublicKeyEncrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.Security;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.SecretKey;

/**
 * Created by wavegame9 on 3/2/2016.
 */
public class RecyclerLoginAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = RecyclerLoginAdapter.class.getSimpleName();

    private List<String> mItemList = new ArrayList<>();
    private Context mContext;
    private Fragment mFragment;
    private CallbackManager mCallbackManager;
    private FacebookCallback<LoginResult> mCallback;

    public RecyclerLoginAdapter(Fragment fragment, CallbackManager callbackManager, FacebookCallback<LoginResult> callback){
        mFragment = fragment;
        mCallbackManager = callbackManager;
        mCallback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_login_item, parent, false);
        return new LoginItemHolder(view);
    }

    public void addItem(String item){
        mItemList.add(item);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final LoginItemHolder mHolder = (LoginItemHolder) holder;
        mContext = mHolder.itemView.getContext();

        mHolder.getBtn_sign_in().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String masterPass = "password";
                Security satpam = new Security();
                String newSalt = "";
                try {
                    newSalt = satpam.generateSalt();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Generate SecretKey
                SecretKey secretKey;
                String encodedKey = null;
                try {
                    secretKey = Security.getSecretKey(masterPass, newSalt);
//                    encodedKey = Base64.encodeToString(secretKey.getEncoded(), Base64.DEFAULT);
                    encodedKey = new String(org.apache.commons.codec.binary.Base64.encodeBase64(secretKey.getEncoded(), false), "UTF-8");
                    if (encodedKey.length() == 25)
                        encodedKey = encodedKey.substring(0, encodedKey.length() - 1);
                    else if (encodedKey.length() > 32)
                        encodedKey = encodedKey.substring(0, 32);
                    Log.d(TAG, "secretKey encrypt= " + encodedKey);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "error generate secretKey saat Encrypt: " + e.getMessage());
                }

//
//
//                //proses encrypt
//                SecretKey secretKey;
//                String encodedKey="wK9IeaRdkyPuumThYx8Fvg6AfJfCwgCj6Zy0Hplf6VQ=";
//                try {
//                    secretKey = RahasiaSekali.getSecretKey(masterPass, newSalt);
//                    EncryptedText = RahasiaSekali.encrypt(secretKey, "{\"datausrname\":\"hendra6@yahoo.com\",\"datausrpass\":\"hendra\",\"datausrpass2\":\"hendra\",\"datausrgmail\":\"foo@gmail.com\"}");
//                    encodedKey = MyBase64.encodeToString(secretKey.getEncoded(), MyBase64.DEFAULT);
//                    Log.d(TAG, "secretKey encrypt= "+ encodedKey);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.d(TAG, "error generate secretKey saat Encrypt: " + e.getMessage());
//                }
//
//                Log.d(TAG,"Hasil encrypsi : "+EncryptedText);

                // decode the base64 encoded string


                //String encodedKey = "mL2yOhusRwiBcl2Wel3FPSbkC/ZcA1hLwSSYnr5cJUA=";
                //String EncryptedText = "7DD448222DCD280537D7D0F7626E0735DCFD15B707624A46F17F36D279E20EDF63B08700F23DF40A7170573F6992D27097AAF5D83ECC8C9D4E1E98C5C4BF4D9F4088759F941CBBFED760E4765EA44076F152B82DEF923CADAB4DDD1CC06DA653AE1BB9D676BACD8AB8635E48252366DCBB2128583710CE9C8F4D52AC135FDA022231BE8D99A5606506511144462DE8CE";

//                String encodedKey = "FMw+mMYb1MnZ6CK8geTup52rVd4YdFIk2LGLc2n/ERU=";
//                String EncryptedText = "6P1GiR2LTPjSkrJC-9VDnQ8bN0mVLJduOnfW0BjEifrFeVD6CaOXF9XTP5BuirtfPqzoZSvSHNqk64ieBDT4dkuh7awItdCqxyp1nZFMFxdJDeaUUeEpGua3XticMuZO5GQSvY-0x35GyQPoCdRdw6ubOlm9YC1bRp4-QQ5xRWk";
//
//
//                String DecryptedText ="";
//                byte[] decodedKey = MyBase64.decode(encodedKey,MyBase64.DEFAULT);
//                // rebuild key using SecretKeySpec
//                SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
//                Log.d(TAG, "lengh decodedKey = "+decodedKey.length);
//                //proses decrypt
//                try {
//                    DecryptedText = RahasiaSekali.decrypt(originalKey, EncryptedText);
//                    Log.d(TAG, "secretKey decrypt= "+ MyBase64.encodeToString(originalKey.getEncoded(), MyBase64.DEFAULT));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.d(TAG, "error generate secretKey saat Decrypt: " + e.getMessage());
//                }
//                Toast.makeText(mContext,"Hasil encrypsi : "+EncryptedText, Toast.LENGTH_LONG).show();
//                Log.d(TAG,"Hasil decrypsi : "+DecryptedText);





            }
        });
        //end of sign in click event

        mHolder.getBtn_fb_sign_in().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String masterPass = "password";
//                String input = "{\"datausrname\":\"hendra0@yahoo.com\",\"datausrpass\":\"hendra\",\"datausrpass2\":\"hendra\",\"datausrgmail\":\"foo0@gmail.com\"}";
//
//                String EncryptedText=null;
//                try {
//                    EncryptedText = SimpleCrypto.encrypt(masterPass, input);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Toast.makeText(mContext,"Hasil encryption : "+EncryptedText, Toast.LENGTH_LONG).show();
//                Log.d(TAG, "Hasil encryption : " + EncryptedText);
//
//                String DecryptedText = null;
//                try {
//                    DecryptedText = SimpleCrypto.decrypt(masterPass, EncryptedText);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Toast.makeText(mContext,"Hasil Decryption : "+DecryptedText, Toast.LENGTH_LONG).show();
//                Log.d(TAG, "Hasil decryption : " + DecryptedText);


//                PrivateKeyEncrypt.show();
//                PublicKeyDecrypt.show();
//
//                String input = "{\"datausrname\":\"hendra6@yahoo.com\",\"datausrpass\":\"hendra\",\"datausrpass2\":\"hendra\",\"datausrgmail\":\"foo@gmail.com\"}";
//                Log.d(TAG, input);
//                String output = SimpleCrypto.toHex(input);
//                Log.d(TAG, output);
//
//                Log.d(TAG, new String(SimpleCrypto.toByte(output)));


                //1. Generate Random SecretKey :
                String masterPass = "password";
                Security satpam = new Security();
                //Generate Salt
                String newSalt = "";
                try {
                    newSalt = satpam.generateSalt();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SecretKey generatedKey = null;
                //------- dikirim ke servser ----------
                String randomSecretKey = "";
                //-------------------------------------
                try {
                    generatedKey = Security.getSecretKey(masterPass, newSalt);
                    randomSecretKey = new String(Base64.encode(generatedKey.getEncoded(), Constants.HTTP.BASE64_MODE), "UTF-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "error generate secretKey saat Encrypt: " + e.getMessage());
                }
                Key aesKey = Security.getAESKey(randomSecretKey);

                try {
                    Log.d(TAG, "Random SecretKey = " + URLEncoder.encode(randomSecretKey, "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //2. Generate IV
                byte[] iv = null;
                try {
                    iv = Security.generateIv();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (NoSuchProviderException e) {
                    e.printStackTrace();
                }
                //------- dikirim ke servser ----------
                String ivString = "";
                //------- dikirim ke servser ----------
                try {
                    ivString = new String(Base64.encode(iv, Constants.HTTP.BASE64_MODE), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                try {
                    Log.d(TAG, "ivEncoded = " + URLEncoder.encode(ivString, "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                //3. Encrypt The Input
//                String input = "I'M AN ABDUL RAHMAN";
                String input = "{\"datausrname\":\"hendra6@yahoo.com\",\"datausrpass\":\"hendra\",\"datausrpass2\":\"hendra\",\"datausrgmail\":\"foo@gmail.com\"}";
//
                Log.d(TAG, "input AES = " + input);
                //------- dikirim ke servser ----------
                String outputAES = null;
                //------- dikirim ke servser ----------
                try {
                    outputAES = Security.encryptCBC(aesKey, iv, input);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Log.d(TAG, "output AES = " + URLEncoder.encode(outputAES, "utf-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                //-------------------------------------------------------------------------------------------------
//                String aesKeyDecrypt = "aeskeybutuhstringpajangminimal32";
//                // String encryptedAESFromPHP = "yqxoTlkPNr+PRGDk0pISlg==:2IPmN0ANSD7wgFPqFT7PDs0tH5IXTtb6UiZX2sUBjCM="; // PHP with OpenSSL
//                String encryptedAESFromPHP = "h308MAFBiR+Uh+wWdHfoaQ==:pjFaB1sTXLYAf4Ekrf/YII3fI3yGybNGk2FVXvFRInE="; // PHP with MCrypt
//                String [] parts = encryptedAESFromPHP.split(":");
//                String ivDecrypt = parts[0];
//                String encryptedAES = parts[1];
//
//                byte[] ivBytes = Base64.decode(ivDecrypt, Base64.DEFAULT);
//                byte[] encryptedAESBytes = Base64.decode(encryptedAES, Base64.DEFAULT);
//                byte[] aesKeyBytes = aesKeyDecrypt.getBytes();
//
//                String decryptedTextAesPlain = null;
//                try {
//                    Key keySpecAes = new SecretKeySpec(aesKeyBytes, "AES");
//                    IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
//                    Cipher cipheer = Cipher.getInstance("AES/CBC/PKCS5Padding");
//                    cipheer.init(Cipher.DECRYPT_MODE, keySpecAes, ivSpec);
//                    byte[] decryptedTextAes = null;
//                    decryptedTextAes = cipheer.doFinal(encryptedAESBytes);
//                    decryptedTextAesPlain = new String(decryptedTextAes, "UTF-8");
//                } catch (Exception e) {
//                    throw new RuntimeException(e.getMessage());
//                }
//
//                Log.d("Decrypted text: ", decryptedTextAesPlain);
                String hasilDecryptAES = null;
                try {
                    hasilDecryptAES = Security.decryptCBC(randomSecretKey, ivString, outputAES);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "HASIL DECRYPT = " + hasilDecryptAES);

//                try {
//                    hasilDecryptAES = Security.decryptCBC("qb/YwRz9IR0hFOrPm3e4YENrWmGLHOyjSGrD8MdSJ2Y=","m8ZR1LZqIB4ouFwJW/hRzg==","a0AtCs4JpW2Yt9KefxXE8wHNQmxCKr9hktv3qBLVd40=");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Log.d(TAG,"HASIL DECRYPT MANUAL = "+hasilDecryptAES);


                Log.d(TAG, "=======================================================================================================");
                //PublicKeyEncrypt.show(randomSecretKey);
                PrivateKeyDecrypt.show(PublicKeyEncrypt.show(randomSecretKey));
                Log.d(TAG, "=======================================================================================================");


                mContext = holder.itemView.getContext();

                String appPackageName = "com.woi.liputan6.android"; // getPackageName() from Context or Activity object
                //panggil google play untuk install
//                try {
//                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                }
//                catch (android.content.ActivityNotFoundException anfe) {
//                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
//                }

                //cek apakah suatu package sudah terinstall apa belum
                int flags = PackageManager.GET_META_DATA |
                        PackageManager.GET_SHARED_LIBRARY_FILES |
                        PackageManager.GET_UNINSTALLED_PACKAGES;

                PackageManager pm = mContext.getPackageManager();
                List<ApplicationInfo> applications = pm.getInstalledApplications(flags);
                for (ApplicationInfo appInfo : applications) {
                    Log.d(TAG, appInfo.packageName);
                    if (appInfo.packageName.equalsIgnoreCase(appPackageName)) {
                        Log.d(TAG, "ALREADY INSTALLED");
                        break;
                    }
//                    if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
//                        // System application
//                    } else {
//                        // Installed by user
//                    }
                }

                try {
                    long installed = mContext
                            .getPackageManager()
                            .getPackageInfo("com.catfiz", 0)
                            .firstInstallTime;
                    Log.d(TAG, "INSTALLED CATFIZ AT : " + installed);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }


                //trying to get deviceId
                DeviceProperties deviceProperties = DeviceProperties.getInstance(mContext);

                //1. deviceOs=[DEVICE_OS]
                Log.d(TAG, "DEVICE_OS= " + deviceProperties.getDeviceOsType());

                //2. &deviceIds[TYPE]=[DEVICE_ID]
                for (String deviceIdType : deviceProperties.getDeviceIds().keySet()) {
                    Log.d(TAG, "TYPE= " + deviceProperties.getDeviceOsType());
                    Log.d(TAG, "DEVICE_ID= " + deviceProperties.getDeviceIds().get(deviceIdType));
                }
                //[TYPE] should be either IFA for iOS or AID for Android.
                Log.d(TAG, "DEVICE_ID= " + deviceProperties.getDeviceIds().get("AID"));

                // &isLimitAdTrackingEnabled=[true/false]

                //3. &deviceOSVersion=[osVersion]
                Log.d(TAG, "osVersion= " + Integer.toString(deviceProperties.getDeviceOsVersion()));
            }
        });

        mHolder.getLoginButton().setReadPermissions("public_profile");
        mHolder.getLoginButton().setReadPermissions("user_friends");
        mHolder.getLoginButton().setReadPermissions("email");
//        mHolder.getLoginButton().setReadPermissions("user_photos");
        mHolder.getLoginButton().setFragment(mFragment);
        mHolder.getLoginButton().registerCallback(mCallbackManager, mCallback);

        mHolder.getSignInButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FreepointMainActivity) mContext).signIn();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
