package com.wavegame.freepoint.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.adapter.RecyclerPremiumAppAdapter;
import com.wavegame.freepoint.wavegamesdk.model.AppItem;
import com.wavegame.freepoint.wavegamesdk.restmanager.AppItemRestManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class ListAppFragment extends Fragment {

    private static final String TAG = ListAppFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerPremiumAppAdapter mAppAdapter;
    private AppItemRestManager mManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootFragment = inflater.inflate(R.layout.fragment_listapp,container,false);

        configView(rootFragment);

        mManager = new AppItemRestManager();
        Call<List<AppItem>> listCall = mManager.getAppItemService().getPremiumApps();
        listCall.enqueue(new Callback<List<AppItem>>() {
            @Override
            public void onResponse(Call<List<AppItem>> call, Response<List<AppItem>> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "faruk : " + response.body().toString());
                    List<AppItem> appList = response.body();
                    mAppAdapter.addListApp(appList);

                } else {
                    Log.d(TAG, "faruk gagal");
                    int sc = response.code();
                    switch (sc) {

                    }
                }
            }

            @Override
            public void onFailure(Call<List<AppItem>> call, Throwable t) {
                Log.d(TAG, "faruk onFailure :" + t.getMessage());
            }
        });
        return rootFragment;
    }

    private void configView(View rootFragment) {
        //listing item
        mRecyclerView = (RecyclerView) rootFragment.findViewById(R.id.premiumRecyclerView);
        setupRecyclerView(mRecyclerView);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        mAppAdapter = new RecyclerPremiumAppAdapter();
        recyclerView.setAdapter(mAppAdapter);
    }

}
