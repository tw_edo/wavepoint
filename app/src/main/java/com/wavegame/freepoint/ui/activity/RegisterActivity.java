package com.wavegame.freepoint.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.control.JSONControl;
import com.wavegame.freepoint.model.ModelUser;
import com.wavegame.freepoint.utilities.ApplicationManager;
import com.wavegame.freepoint.utilities.DialogManager;
import com.wavegame.freepoint.utilities.UserEmailFetcher;
import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.rahasia.MCrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.PrivateKeyDecrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.PublicKeyDecrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.PublicKeyEncrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.Security;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;

import javax.crypto.SecretKey;

import static java.nio.charset.StandardCharsets.*;

/**
 * Created by wavegame9 on 3/1/2016.
 */
public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    private TextView btnSignin;
    private EditText txtEmail, txtPassword, txtConfirmPassword;
    private String username, password, confirmPassword, gmail;
    private Activity mActivity;
    private UserEmailFetcher emailFetcher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mActivity = this;
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnSignin = (TextView) findViewById(R.id.btnSignin);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);
        emailFetcher = new UserEmailFetcher();

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = txtEmail.getText().toString();
                password = txtPassword.getText().toString();
                confirmPassword = txtConfirmPassword.getText().toString();
                gmail = emailFetcher.getEmail(mActivity);

                String input_dataUsr = "{\"datausrname\":\""+username+
                        "\",\"datausrpass\":\""+password+
                        "\",\"datausrpass2\":\""+confirmPassword+
                        "\",\"datausrgmail\":\""+gmail+"\"}";

                com.wavegame.freepoint.wavegamesdk.helper.Log.d(TAG, "input_dataUsr = " + input_dataUsr);

                try {
                    MCrypt mcrypt = new MCrypt();
                    String a = URLEncoder.encode(new String(Base64.encode(mcrypt.encrypt(input_dataUsr),Base64.DEFAULT)), "UTF-8");
                    String b = URLEncoder.encode(new String(Base64.encode(mcrypt.getEncryptedAES(), Base64.DEFAULT)), "UTF-8");
                    String c = URLEncoder.encode(new String(Base64.encode(mcrypt.getIv().getBytes(), Base64.DEFAULT)), "UTF-8");
                    Log.d("EDO","a = "+ a);
                    Log.d("EDO","b = "+ b);
                    Log.d("EDO","c = "+ c);
                    new DoRegister(mActivity).execute(a, b, c);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
      public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }


    private class DoRegister extends AsyncTask<String, Void, String> {
        private String message = "Tidak dapat registrasi";
        private Activity activity;
        private Context context;
        private Resources resources;
        private ProgressDialog progressDialog;
        String error = "";

        public DoRegister(Activity activity) {
            super();
            this.activity = activity;
            this.context = activity.getApplicationContext();
            this.resources = activity.getResources();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Registering. . .");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String a = params[0];
                String b = params[1];
                String c = params [2];

                JSONControl jsControl = new JSONControl();
                JSONObject responseRegister = jsControl.postRegister(a,b,c);
                Log.d("json responseRegister", responseRegister.toString());
                if (!responseRegister.toString().contains("errors")) {
                    ModelUser user = new ModelUser();
                    String _id = responseRegister.getString("accid");
                    String _name = responseRegister.getString("usrnm");
                    String _email = "";
                    String _freepoin = responseRegister.getString("freepoin");
                    String _token1 = responseRegister.getString("token1");
                    String _token2 = responseRegister.getString("token2");
                    String _token3 = responseRegister.getString("token3");
                    String stat = responseRegister.getString("stat");
                    user.setId(_id);
                    user.setNama(_name);
                    user.setEmail(_email);
                    ApplicationManager.getInstance(activity).setUser(user);
                    ApplicationManager.getInstance(activity).setFreepoin(_freepoin);
                    ApplicationManager.getInstance(activity).setUserToken1(_token1);
                    ApplicationManager.getInstance(activity).setUserToken2(_token2);
                    ApplicationManager.getInstance(activity).setUserToken3(_token3);
                    message = stat;
                    return "OK";
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return "FAIL";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();
            switch (result) {
                case "FAIL":
                    DialogManager.showDialog(mActivity, "Mohon Maaf", message);
                    break;

                case "OK":
                    Intent i = new Intent(getBaseContext(), FreepointMainActivity.class);
                    startActivity(i);
                    finish();
                    break;
            }
        }


    }



}
