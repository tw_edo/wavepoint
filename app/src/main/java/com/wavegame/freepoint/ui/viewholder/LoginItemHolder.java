package com.wavegame.freepoint.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 3/2/2016.
 */
public class LoginItemHolder extends RecyclerView.ViewHolder {

    private LoginButton loginButton;
    private SignInButton signInButton;
    private Button btn_sign_in;
    private Button btn_fb_sign_in;
    private EditText et_email;

    public LoginItemHolder(View itemView) {
        super(itemView);
        btn_sign_in = (Button) itemView.findViewById(R.id.btn_sign_in);
        et_email = (EditText) itemView.findViewById(R.id.et_email);
        btn_fb_sign_in =  (Button) itemView.findViewById(R.id.btn_fb_sign_in);


        loginButton = (LoginButton) itemView.findViewById(R.id.login_button);

        signInButton = (SignInButton) itemView.findViewById(R.id.google_sign_in_button);
        signInButton.setColorScheme(SignInButton.COLOR_DARK);

    }

    public EditText getEt_email() {
        return et_email;
    }

    public Button getBtn_sign_in() {
        return btn_sign_in;
    }
    public Button getBtn_fb_sign_in() {
        return btn_fb_sign_in;
    }

    public LoginButton getLoginButton() {
        return loginButton;
    }

    public SignInButton getSignInButton() {
        return signInButton;
    }
}
