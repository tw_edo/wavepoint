package com.wavegame.freepoint.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.viewholder.DummyItemHolder;

import java.util.List;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mItemList;

    public RecyclerAdapter(List<String> itemList) {
        mItemList = itemList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_dummy_item, parent, false);

        return DummyItemHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DummyItemHolder vHolder = (DummyItemHolder) holder;
        vHolder.setTvDummyText(mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

}
