package com.wavegame.freepoint.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class DummyItemHolder extends RecyclerView.ViewHolder {
    private final TextView tv_dummy_item;

    public DummyItemHolder(final View parent, TextView textView) {
        super(parent);
        tv_dummy_item  = textView;
    }

    public static DummyItemHolder newInstance(View parent){
        TextView textView = (TextView) parent.findViewById(R.id.tv_dummy_item);
        return new DummyItemHolder(parent, textView);
    }

    public void setTvDummyText(CharSequence text){
        tv_dummy_item.setText(text);
    }

}
