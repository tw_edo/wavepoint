package com.wavegame.freepoint.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 2/25/2016.
 */
public class PremiumAppHolder extends RecyclerView.ViewHolder {

    //application name
    private TextView tv_app_name;
    //application resume earning (total posible earning)
    private TextView tv_app_to_earn;
    //application picture
    private ImageView iv_app_pic_bg;
    //application icon
    private ImageView iv_app_icon;
//    private CircularImageView iv_app_icon;

//    public CircularImageView getIv_app_icon() {
//        return iv_app_icon;
//    }

    public TextView getTv_app_name() {
        return tv_app_name;
    }

    public TextView getTv_app_to_earn() {
        return tv_app_to_earn;
    }

    public ImageView getIv_app_pic_bg() {
        return iv_app_pic_bg;
    }

    public ImageView getIv_app_icon() {
        return iv_app_icon;
    }

    public TextView getTv_install_reward() {
        return tv_install_reward;
    }

    public TextView getTv_app_desc() {
        return tv_app_desc;
    }

    public Button getBtn_install() {
        return btn_install;
    }

    public TextView getTv_dailyplay_reward() {
        return tv_dailyplay_reward;
    }

    public TextView getTv_dailyplay_minute() {
        return tv_dailyplay_minute;
    }

    public Button getBtn_dailyplay() {
        return btn_dailyplay;
    }

    public TextView getTv_complete_daily_reward() {
        return tv_complete_daily_reward;
    }

    public Button getBtn_complete_daily() {
        return btn_complete_daily;
    }

    public TextView getTv_notunin_reward() {
        return tv_notunin_reward;
    }

    public TextView getTv_notunin_desc() {
        return tv_notunin_desc;
    }

    /*
        for install row
         */
    //application install
    private TextView tv_install_reward;
    //application description
    private TextView tv_app_desc;
    //application button install
    private Button btn_install;


    /*
    for daily Play row
     */
    //application dailyplay reward
    private TextView tv_dailyplay_reward;
    //application minute dailyplay
    private TextView tv_dailyplay_minute;
    //application button dailyplay
    private Button btn_dailyplay;

    /*
    for completing daily play
     */
    //application complete daily reward
    private TextView tv_complete_daily_reward;
    //application button complete daily
    private Button btn_complete_daily;

    /*
    for not uninstall
     */
    //application not_unin reward
    private TextView tv_notunin_reward;
    //application text not_unin
    private TextView tv_notunin_desc;


    public PremiumAppHolder(View itemView) {
        super(itemView);

        tv_app_name = (TextView) itemView.findViewById(R.id.tv_app_name);
        tv_app_to_earn = (TextView) itemView.findViewById(R.id.tv_app_to_earn);
        iv_app_pic_bg = (ImageView) itemView.findViewById(R.id.iv_app_pic_bg);
        iv_app_icon = (ImageView) itemView.findViewById(R.id.iv_app_icon);
//        iv_app_icon = (CircularImageView) itemView.findViewById(R.id.iv_app_icon);

        tv_install_reward = (TextView) itemView.findViewById(R.id.tv_install_reward);
        tv_app_desc = (TextView) itemView.findViewById(R.id.tv_app_description);
        btn_install = (Button) itemView.findViewById(R.id.buttonInstall);

        tv_dailyplay_reward = (TextView) itemView.findViewById(R.id.tv_dailyplay_reward);
        tv_dailyplay_minute = (TextView) itemView.findViewById(R.id.tv_dailyplay_minute);
        btn_dailyplay = (Button) itemView.findViewById(R.id.buttonDailyPlay);

        tv_complete_daily_reward = (TextView) itemView.findViewById(R.id.tv_complete_daily_reward);
        btn_complete_daily = (Button) itemView.findViewById(R.id.buttonCompleteDaily);

        tv_notunin_reward = (TextView) itemView.findViewById(R.id.tv_notunin_reward);
        tv_notunin_desc = (TextView) itemView.findViewById(R.id.tv_notunin_perday);

    }


}
