package com.wavegame.freepoint.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.activity.FreepointMainActivity;
import com.wavegame.freepoint.ui.adapter.RecyclerLoginAdapter;
import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;
import com.wavegame.freepoint.wavegamesdk.helper.Util;

/**
 * Created by wavegame9 on 3/2/2016.
 */
public class LoginFragment extends Fragment {
    private static final String TAG = LoginFragment.class.getSimpleName();
    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    static FreepointMainActivity.FirstPageFragmentListener firstPageListener;

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d(TAG, "masuk success");
            AccessToken accessToken = loginResult.getAccessToken();
            Log.d(TAG, "ACCESS TOKEN = "+accessToken.toString());
            Profile profile = Profile.getCurrentProfile();
            displayMessage(profile);
        }

        @Override
        public void onCancel() {
            Log.d(TAG, "masuk cancel");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d(TAG, "masuk error");
        }
    };


    public static LoginFragment createInstance(FreepointMainActivity.FirstPageFragmentListener listener){
        LoginFragment loginFragment = new LoginFragment();
        loginFragment.firstPageListener = listener;
        return loginFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker= new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

                Log.d(TAG, "New Access Token = " + newToken.getToken());
                Log.d(TAG, "New User Id = " + newToken.getUserId());
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                if(newProfile!= null)
                    displayMessage(newProfile);
                else
                    Log.d(TAG, "NEW PROFILE FB IS NULL");
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();



        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_login,container,false);
        setupRecyclerView(recyclerView);
        return recyclerView;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerLoginAdapter recyclerAdapter = new RecyclerLoginAdapter(this,callbackManager,callback);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.addItem("Login");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"masuk onActivityResult");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void displayMessage(Profile profile){
        if(profile != null){
            Log.d(TAG, "NAME FB = " + profile.getName());
            String newName = profile.getName();
            Util.putPrefString(Constants.PREFERENCE.PROFILE_NAME, newName, getActivity().getApplicationContext());
            Util.putPrefString(Constants.PREFERENCE.PROFILE_ID, profile.getId(), getActivity().getApplicationContext());
            Util.putPrefString(Constants.PREFERENCE.PROFILE_PIC_URI, profile.getProfilePictureUri(120,120).toString(), getActivity().getApplicationContext());
            firstPageListener.onSwitchToMenuFragment(newName);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

}
