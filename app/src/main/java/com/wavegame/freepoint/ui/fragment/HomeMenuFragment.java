package com.wavegame.freepoint.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.activity.LoginActivity;
import com.wavegame.freepoint.ui.activity.SupersonicActivity;
import com.wavegame.freepoint.ui.adapter.RecyclerHomeMenuAdapter;
import com.wavegame.freepoint.utilities.DialogManager;
import com.wavegame.freepoint.wavegamesdk.model.HomeMenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class HomeMenuFragment extends Fragment {
    private static final String TAG = HomeMenuFragment.class.getSimpleName();
    private ListView mListView;
    private TextView txtBalance;
    private RelativeLayout btnFreePoint, btnTukarWP, btnTopUp, btnMerchant, btnLaporan, btnProfile;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_guide, container, false);
        List<HashMap<String, String>> fillMaps = new ArrayList<HashMap<String, String>>();
        for(int i = 0; i < 1; i++){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("rowid", "" + i);
            map.put("col_1", "col_1_item_" + i);
            map.put("col_2", "col_2_item_" + i);
            map.put("col_3", "col_3_item_" + i);
            fillMaps.add(map);
        }
        View v = inflater.inflate(R.layout.activity_guide, container, false);
        mListView = (ListView) v.findViewById(R.id.listGuide);
        View empty = getActivity().getLayoutInflater().inflate(R.layout.guide_empty, null);
        mListView.setEmptyView(empty);
        View header = getActivity().getLayoutInflater().inflate(R.layout.guide_header, null);
        txtBalance = (TextView) header.findViewById(R.id.txtBalance);
        btnFreePoint = (RelativeLayout) header.findViewById(R.id.btnFreePoint);
        btnTukarWP = (RelativeLayout) header.findViewById(R.id.btnTukarWP);
        btnTopUp = (RelativeLayout) header.findViewById(R.id.btnTopUp);
        btnMerchant = (RelativeLayout) header.findViewById(R.id.btnMerchant);
        btnLaporan = (RelativeLayout) header.findViewById(R.id.btnLaporan);
        btnProfile = (RelativeLayout) header.findViewById(R.id.btnProfile);
        mListView.addHeaderView(header);
        SimpleAdapter myAdapter=new SimpleAdapter(getActivity(),fillMaps,R.layout.grid_item,
                new String[] {"transaction_date_time","user_name","site_name","machine_name"},new int[] {1,2,3});
        mListView.setAdapter(myAdapter);
        //setupRecyclerView(recyclerView);

        btnFreePoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DialogManager.showDialog(getActivity(),"Informasi", "Free Point");
                Intent intent = new Intent(getActivity(),SupersonicActivity.class);
                startActivity(intent);
            }
        });
        btnTukarWP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogManager.showDialog(getActivity(),"Informasi", "Tukar WP");
            }
        });
        btnTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogManager.showDialog(getActivity(),"Informasi", "Top Up");
            }
        });
        btnMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogManager.showDialog(getActivity(),"Informasi", "Merchant");
            }
        });
        btnLaporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogManager.showDialog(getActivity(),"Informasi", "Laporan");
            }
        });
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogManager.showDialog(getActivity(),"Informasi", "Profile");
            }
        });


        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListView.setAdapter(null);
    }


}
