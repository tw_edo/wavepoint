package com.wavegame.freepoint.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.viewholder.DummyItemHolder;
import com.wavegame.freepoint.ui.viewholder.LaporanItemHolder;

import java.util.List;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class RecyclerLaporanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mItemList;

    public RecyclerLaporanAdapter(List<String> itemList) {
        mItemList = itemList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_laporan_item, parent, false);

        return LaporanItemHolder.newInstance(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LaporanItemHolder vHolder = (LaporanItemHolder) holder;
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

}
