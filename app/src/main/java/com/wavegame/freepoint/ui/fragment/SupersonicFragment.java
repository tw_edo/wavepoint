package com.wavegame.freepoint.ui.fragment;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.supersonic.adapters.supersonicads.SupersonicConfig;
import com.supersonic.mediationsdk.logger.SupersonicError;
import com.supersonic.mediationsdk.model.Placement;
import com.supersonic.mediationsdk.sdk.InterstitialListener;
import com.supersonic.mediationsdk.sdk.OfferwallListener;
import com.supersonic.mediationsdk.sdk.RewardedVideoListener;
import com.supersonic.mediationsdk.sdk.Supersonic;
import com.supersonic.mediationsdk.sdk.SupersonicFactory;
import com.supersonic.mediationsdk.utils.SupersonicUtils;
import com.supersonicads.sdk.agent.SupersonicAdsAdvertiserAgent;
import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 3/31/2016.
 */
public class SupersonicFragment extends Fragment implements RewardedVideoListener, OfferwallListener, InterstitialListener {
    private static final String TAG = SupersonicFragment.class.getSimpleName();

    private Button mVideoButton;
    private Button mOfferwallButton;
    private Button mInterstitialButton;
    private Supersonic mSupersonicInstance;

    private Placement mPlacement;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String userId = "userIdTestOverwall";
        // String appKey = "3be516e9";
        String appKey = "4655b54d";
        // create the supersonic instance - this should be called when the activity starts
        mSupersonicInstance = SupersonicFactory.getInstance();


        // Supersonic Advertiser SDK call
        SupersonicAdsAdvertiserAgent.getInstance().reportAppStarted(getContext());
        // Be sure to set a listener to each product that is being initiated
        // set the Supersonic rewarded video listener
        mSupersonicInstance.setRewardedVideoListener(this);
        // init the supersonic rewarded video
        mSupersonicInstance.initRewardedVideo(getActivity(), appKey, userId);
        // set the Supersonic offerwall listener
        mSupersonicInstance.setOfferwallListener(this);
        // init the supersonic offerwall
        // set client side callbacks for the offerwall
        SupersonicConfig.getConfigObj().setClientSideCallbacks(true);
        mSupersonicInstance.initOfferwall(getActivity(), appKey, userId);
        // set the interstitial listener
        mSupersonicInstance.setInterstitialListener(this);
        // init the supersonic interstitial
        mSupersonicInstance.initInterstitial(getActivity(), appKey, userId);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_supersonic,container, false);
        initUIElements(mView);
        return mView;
    }

    private void initUIElements(View v){
        mVideoButton = (Button) v.findViewById(R.id.rv_button);
        mVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if video is available
                if (mSupersonicInstance.isRewardedVideoAvailable())
                    //show rewarded video
                    mSupersonicInstance.showRewardedVideo();
            }
        });

        mOfferwallButton = (Button) v.findViewById(R.id.ow_button);
        mOfferwallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //show the offerwall
                if (mSupersonicInstance.isOfferwallAvailable())
                    mSupersonicInstance.showOfferwall();
            }
        });

        mInterstitialButton = (Button) v.findViewById(R.id.is_button);
        mInterstitialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if interstitial is available
                if (mSupersonicInstance.isInterstitialAdAvailable())
                    //show the interstitial
                    mSupersonicInstance.showInterstitial();
            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        // call the supersonic onResume method
        if (mSupersonicInstance != null)
            mSupersonicInstance.onResume(getActivity());
        updateButtonsState();
    }

    @Override
    public void onPause(){
        super.onPause();
        // call the supersonic onPause method
        if (mSupersonicInstance != null)
            mSupersonicInstance.onPause(getActivity());
        updateButtonsState();
    }

    /**
     * Handle the button state according to the status of the supersonic producs
     */
    private void updateButtonsState(){
        if (mSupersonicInstance != null){
            handleVideoButtonState(mSupersonicInstance.isRewardedVideoAvailable());
            handleInterstitialButtonState(mSupersonicInstance.isInterstitialAdAvailable());
            handleOfferwallButtonState(mSupersonicInstance.isOfferwallAvailable());
        }
        else{
            handleVideoButtonState(false);
            handleInterstitialButtonState(false);
            handleOfferwallButtonState(false);
        }
    }

    /**
     * Set the Rewareded Video button state according to the product's state
     * @param available if the video is available
     */
    public void handleVideoButtonState(final boolean available){
        final String text;
        final int color;
        if(available)
        {
            color = Color.BLUE;
            text = getResources().getString(R.string.show) + " " +getResources().getString(R.string.rv);
        }
        else
        {
            color = Color.BLACK;
            text = getResources().getString(R.string.initializing) + " " +getResources().getString(R.string.rv);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mVideoButton.setTextColor(color);
                mVideoButton.setText(text);
                mVideoButton.setEnabled(available);

            }
        });
    }

    /**
     * Set the Rewareded Video button state according to the product's state
     * @param available if the offerwall is available
     */
    public void handleOfferwallButtonState(final boolean available){
        final String text;
        final int color;
        if(available)
        {
            color = Color.BLUE;
            text = getResources().getString(R.string.show) + " " +getResources().getString(R.string.ow);
        }
        else
        {
            color = Color.BLACK;
            text = getResources().getString(R.string.initializing) + " " +getResources().getString(R.string.ow);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mOfferwallButton.setTextColor(color);
                mOfferwallButton.setText(text);
                mOfferwallButton.setEnabled(available);

            }
        });

    }

    /**
     * Set the Interstitial button state according to the product's state
     * @param available if the interstitial is available
     */
    public void handleInterstitialButtonState(final boolean available){
        final String text;
        final int color;
        if(available)
        {
            color = Color.BLUE;
            text = getResources().getString(R.string.show) + " " +getResources().getString(R.string.is);
        }
        else
        {
            color = Color.BLACK;
            text = getResources().getString(R.string.initializing) + " " +getResources().getString(R.string.is);

        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mInterstitialButton.setTextColor(color);
                mInterstitialButton.setText(text);
                mInterstitialButton.setEnabled(available);
            }
        });

    }

    //Start Video Listener
    @Override
    public void onRewardedVideoInitSuccess() {

    }

    @Override
    public void onRewardedVideoInitFail(SupersonicError supersonicError) {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onVideoAvailabilityChanged(boolean b) {

    }

    @Override
    public void onVideoStart() {

    }

    @Override
    public void onVideoEnd() {

    }

    @Override
    public void onRewardedVideoAdRewarded(Placement placement) {

    }

    @Override
    public void onRewardedVideoShowFail(SupersonicError supersonicError) {

    }
    //End of Video Listener

    //Start of Offerwall Listener
    @Override
    public void onOfferwallInitSuccess() {

    }

    @Override
    public void onOfferwallInitFail(SupersonicError supersonicError) {

    }

    @Override
    public void onOfferwallOpened() {

    }

    @Override
    public void onOfferwallShowFail(SupersonicError supersonicError) {

    }

    @Override
    public boolean onOfferwallAdCredited(int i, int i1, boolean b) {
        return false;
    }

    @Override
    public void onGetOfferwallCreditsFail(SupersonicError supersonicError) {

    }

    @Override
    public void onOfferwallClosed() {

    }
    //End of OfferWall Listener

    //Start of Interstitial Listener
    @Override
    public void onInterstitialInitSuccess() {

    }

    @Override
    public void onInterstitialInitFail(SupersonicError supersonicError) {

    }

    @Override
    public void onInterstitialAvailability(boolean b) {

    }

    @Override
    public void onInterstitialShowSuccess() {

    }

    @Override
    public void onInterstitialShowFail(SupersonicError supersonicError) {

    }

    @Override
    public void onInterstitialAdClicked() {

    }

    @Override
    public void onInterstitialAdClosed() {

    }
    //End of Interstittial Listener

    public void showRewardDialog(Placement placement){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setTitle(getResources().getString(R.string.rewarded_dialog_header));
        builder.setMessage(getResources().getString(R.string.rewarded_dialog_message) + " " + placement.getRewardAmount() + " " + placement.getRewardName());
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
