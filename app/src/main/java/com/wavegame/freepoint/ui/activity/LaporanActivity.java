package com.wavegame.freepoint.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.adapter.RecyclerAdapter;
import com.wavegame.freepoint.ui.adapter.RecyclerLaporanAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wavegame9 on 3/1/2016.
 */
public class LaporanActivity extends AppCompatActivity {
    private static final String TAG = LaporanActivity.class.getSimpleName();
    private CallbackManager callbackManager;
    private Button btnLoginEmail;
    private TextView btnRegister;
    private Context mContext;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_laporan);
        recyclerView = (RecyclerView) findViewById(R.id.laporanRecyclerView);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerLaporanAdapter recyclerAdapter = new RecyclerLaporanAdapter(createItemList());
        recyclerView.setAdapter(recyclerAdapter);




    }

    private List<String> createItemList() {
        List<String> itemList = new ArrayList<>();

            int itemsCount = 4;
            for (int i = 0; i < itemsCount; i++) {
                itemList.add("Item " + i);

        }
        return itemList;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LaporanActivity.this,FreepointMainActivity.class);
        startActivity(intent);
        finish();
    }


}
