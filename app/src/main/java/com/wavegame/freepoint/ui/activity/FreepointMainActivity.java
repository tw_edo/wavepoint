package com.wavegame.freepoint.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.fragment.DummyListFragment;
import com.wavegame.freepoint.ui.fragment.HomeMenuFragment;
import com.wavegame.freepoint.ui.fragment.ListAppFragment;
import com.wavegame.freepoint.ui.fragment.LoginFragment;
import com.wavegame.freepoint.utilities.ApplicationManager;
import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.helper.Log;
import com.wavegame.freepoint.wavegamesdk.helper.Util;

import java.util.ArrayList;
import java.util.List;

public class FreepointMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = FreepointMainActivity.class.getSimpleName();
    PagerAdapter mPagerAdapter = null;

    //Signing Options
    private GoogleSignInOptions gso;

    //google api client
    private GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    //ui element
    private ImageView user_picture;
    private RelativeLayout btnLogout;
    private Activity mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;
        configView();
        btnLogout = (RelativeLayout) findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationManager.getInstance(mActivity).setUser(null);
                Intent intent = new Intent(mActivity,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void configView() {
        setContentView(R.layout.activity_freepoint_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //setting viewPager and tabLayout
        ViewPager viewPager = (ViewPager) findViewById(R.id.vp_main);
        String profileName = Util.getPrefString(Constants.PREFERENCE.PROFILE_NAME, getApplicationContext());
        String profileId = Util.getPrefString(Constants.PREFERENCE.PROFILE_ID, getApplicationContext());
        String ppUri = Util.getPrefString(Constants.PREFERENCE.PROFILE_PIC_URI, getApplicationContext());

        View header = navigationView.getHeaderView(0);
        user_picture = (ImageView) header.findViewById(R.id.profile_pic);
        if (ppUri != null && !ppUri.isEmpty()) {
            Log.d(TAG, "ppUri = " + ppUri);
            if (user_picture != null)
                Glide.with(this).load(ppUri).centerCrop().into(user_picture);
        }

        mPagerAdapter = new PagerAdapter(getSupportFragmentManager(), profileName, this);
//        pagerAdapter.addFragment(new HomeMenuFragment(), "Home");
//        pagerAdapter.addFragment(DummyListFragment.createInstance(10), "About Us");
//        pagerAdapter.addFragment(new ListAppFragment(), "Guide");
//        pagerAdapter.addLoginFragment(new LoginFragment(pagerAdapter.listener));
//        pagerAdapter.activateLogin();

        viewPager.setAdapter(mPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tl_main);
        tabLayout.setupWithViewPager(viewPager);


        viewPager.setCurrentItem(0);


        /*GOOGLE THINGS */
        /* GOOGLE SIGN-IN PROCESS */
        //Initializing google signin option
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.freepoint_main, menu);
        if (Util.getPrefString(Constants.PREFERENCE.PROFILE_NAME, getApplicationContext()) == null) {
            menu.findItem(R.id.action_logout).setVisible(false);
        } else {
            menu.findItem(R.id.action_logout).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_logout: {
                LoginManager.getInstance().logOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                // ...
                            }
                        });

                Util.putPrefString(Constants.PREFERENCE.PROFILE_NAME, null, getApplicationContext());
                mPagerAdapter.listener.onSwitchToMenuFragment(null);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    //This function will option signing intent
    public void signIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
    }


    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

//            //Displaying name and email
//            textViewName.setText(acct.getDisplayName());
//            textViewEmail.setText(acct.getEmail());
//
            String newName = acct.getDisplayName();
            Util.putPrefString(Constants.PREFERENCE.PROFILE_NAME, newName, this);
            Util.putPrefString(Constants.PREFERENCE.PROFILE_ID, acct.getId(), this);

            if (acct.getPhotoUrl() != null) {
                String ppUri = acct.getPhotoUrl().toString();
//                Glide.with(this).load(ppUri).centerCrop().into(user_picture);
                Util.putPrefString(Constants.PREFERENCE.PROFILE_PIC_URI, ppUri, this);
            } else {
                Util.putPrefString(Constants.PREFERENCE.PROFILE_PIC_URI, "", this);
            }
            mPagerAdapter.listener.onSwitchToMenuFragment(newName);
        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help) {
            // Handle the camera action
        }
        if (id == R.id.nav_profile) {
            Intent intent = new Intent(FreepointMainActivity.this,ProfileActivity.class);
            startActivity(intent);
            finish();
        }
        if (id == R.id.nav_laporan) {
            // Handle the camera action
            Intent intent = new Intent(FreepointMainActivity.this,LaporanActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();
        private String[] titles = {"Free Points", "Guide"};
        public Fragment mFragmentAtPos0;
        private boolean isAlreadyLogin = false;
        private final FragmentManager mFragmentManager;
        private Context mContext;
        public FirstPageListener listener = new FirstPageListener();
        private String mProfileName = null;

        public PagerAdapter(FragmentManager fm, String profileName, Context context) {
            super(fm);
            mFragmentManager = fm;
            mProfileName = profileName;
            mContext = context;
            Log.d(TAG, "PROFILE NAME = " + mProfileName);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "masuk getItem");
            switch (position) {
               /*
                case 0: // Fragment # 0
                    if (mFragmentAtPos0 == null) {
                        if (mProfileName == null)
                            mFragmentAtPos0 = LoginFragment.createInstance(listener);
                        else
                            mFragmentAtPos0 = new HomeMenuFragment();
                    }
                    return mFragmentAtPos0;
                    */
                case 0: // Fragment # 1
                    return DummyListFragment.createInstance(10);
                case 1:// Fragment # 2
                    return new HomeMenuFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getItemPosition(Object object) {
            if (object instanceof LoginFragment &&
                    mFragmentAtPos0 instanceof HomeMenuFragment) {
                Log.d(TAG, "MASUK SINI 0");
                return POSITION_NONE;
            }
            if (object instanceof HomeMenuFragment &&
                    mFragmentAtPos0 instanceof LoginFragment) {
                Log.d(TAG, "MASUK SINI 1");
                return POSITION_NONE;
            }
            return POSITION_UNCHANGED;
        }

        private final class FirstPageListener implements FirstPageFragmentListener {
            public void onSwitchToMenuFragment(String newName) {
                Log.d(TAG, "sudah masuk switch");
                mProfileName = newName;
                if (newName != null) {
                    mFragmentManager.beginTransaction().remove(mFragmentAtPos0).commit();
                    mFragmentAtPos0 = new HomeMenuFragment();
                } else {
                    mFragmentManager.beginTransaction().remove(mFragmentAtPos0).commit();
                    mFragmentAtPos0 = LoginFragment.createInstance(listener);
                }
                notifyDataSetChanged();
                ((FreepointMainActivity) mContext).invalidateOptionsMenu();
            }
        }

    }

    public interface FirstPageFragmentListener {
        void onSwitchToMenuFragment(String newName);
    }
}
