package com.wavegame.freepoint.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class HomeMenuHolder extends RecyclerView.ViewHolder{

    private ImageView iv_menu_pic;
    private TextView tv_menu_name;

    public HomeMenuHolder(View itemView) {
        super(itemView);
        iv_menu_pic = (ImageView) itemView.findViewById(R.id.iv_menu_pic);
        tv_menu_name = (TextView) itemView.findViewById(R.id.tv_menu_name);
    }


    public ImageView getIv_menu_pic() {
        return iv_menu_pic;
    }

    public TextView getTv_menu_name() {
        return tv_menu_name;
    }


}
