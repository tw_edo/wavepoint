package com.wavegame.freepoint.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.supersonicads.sdk.utils.DeviceProperties;
import com.wavegame.freepoint.R;
import com.wavegame.freepoint.control.JSONControl;
import com.wavegame.freepoint.model.ModelUser;
import com.wavegame.freepoint.utilities.ApplicationManager;
import com.wavegame.freepoint.utilities.DialogManager;
import com.wavegame.freepoint.utilities.UserEmailFetcher;
import com.wavegame.freepoint.wavegamesdk.helper.Constants;
import com.wavegame.freepoint.wavegamesdk.rahasia.MCrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.PrivateKeyDecrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.PublicKeyEncrypt;
import com.wavegame.freepoint.wavegamesdk.rahasia.RahasiaSekali;
import com.wavegame.freepoint.wavegamesdk.rahasia.Security;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.List;

import javax.crypto.SecretKey;

/**
 * Created by wavegame9 on 3/1/2016.
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private CallbackManager callbackManager;
    private Button btnLoginEmail;
    private TextView btnRegister;
    private Context mContext;
    private Activity mActivity;
    private String username, password, gmail;
    private UserEmailFetcher emailFetcher;
    private EditText txtEmail, txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        mActivity = this;
        callbackManager = CallbackManager.Factory.create();
        LoginButton btnLoginFB = (LoginButton) findViewById(R.id.btn_fb);
        btnLoginEmail = (Button) findViewById(R.id.btn_sign_in);
        btnRegister = (TextView) findViewById(R.id.btnRegister);
        emailFetcher = new UserEmailFetcher();
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnLoginEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                username = txtEmail.getText().toString();
                password = txtPassword.getText().toString();
                gmail = emailFetcher.getEmail(mActivity);

                if (!username.isEmpty() || !password.isEmpty()) {
                    String input_dataUsr = "{\"datausrname\":\"" + username +
                            "\",\"datausrpass\":\"" + password +
                            "\",\"datausrgmail\":\"" + gmail + "\"}";

                    com.wavegame.freepoint.wavegamesdk.helper.Log.d(TAG, "input_dataUsr = " + input_dataUsr);

                    try {
                        MCrypt mcrypt = new MCrypt();
                        String a = URLEncoder.encode(new String(Base64.encode(mcrypt.encrypt(input_dataUsr), Base64.DEFAULT)), "UTF-8");
                        String b = URLEncoder.encode(new String(Base64.encode(mcrypt.getEncryptedAES(), Base64.DEFAULT)), "UTF-8");
                        String c = URLEncoder.encode(new String(Base64.encode(mcrypt.getIv().getBytes(), Base64.DEFAULT)), "UTF-8");
                        Log.d("EDO", "a = " + a);
                        Log.d("EDO", "b = " + b);
                        Log.d("EDO", "c = " + c);
                        new DoLoginWP(mActivity).execute(a, b, c);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    DialogManager.showDialog(mActivity, "Mohon Maaf", "Silahkan isi username dan password Anda!");
                }
            }
        });
        btnLoginFB.setOnClickListener(new View.OnClickListener()

                                      {
                                          @Override
                                          public void onClick(View v) {
                                              LoginManager.getInstance().logInWithReadPermissions((Activity) v.getContext(), Arrays.asList("public_profile", "email"));
                                          }
                                      }

        );
        btnLoginFB.registerCallback(callbackManager, new FacebookCallback<LoginResult>()

                {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        // App code
                        Log.d("hasil - ", loginResult.toString());
                        Toast.makeText(getApplicationContext(), "login success!", Toast.LENGTH_SHORT).show();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        Log.d("LoginActivity", object.toString());
                                        //FacebookData data = new FacebookData(object, loginResult.getAccessToken());
                                        //ApplicationData.fbData = data;
                                /*
                                try{
                                    String id = object.getString("id");
                                    String gender = object.getString("gender");
                                    String name = object.getString("name");
                                    String picture = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    Log.d("LoginActivity",id+" - "+name+" - "+gender+" - "+picture);
                                }
                                catch (Exception e){

                                }
                                */
                                        //ApplicationData.menu = 0;
                                        Intent intent = new Intent(getApplicationContext(), FreepointMainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,picture");

                        request.setParameters(parameters);
                        request.executeAsync();

                        //  Log.d("name - ", Profile.getCurrentProfile().getFirstName());


                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("hasil - ", "cancel");
                        Toast.makeText(getApplicationContext(), "login cancel!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("hasil - ", exception.toString());
                        Toast.makeText(getApplicationContext(), "login error!", Toast.LENGTH_SHORT).show();
                    }
                }

        );

    }

    private class DoLoginWP extends AsyncTask<String, Void, String> {
        private String message = "Tidak dapat login";
        private Activity activity;
        private Context context;
        private Resources resources;
        private ProgressDialog progressDialog;
        String error = "";

        public DoLoginWP(Activity activity) {
            super();
            this.activity = activity;
            this.context = activity.getApplicationContext();
            this.resources = activity.getResources();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Signing in. . .");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String a = params[0];
                String b = params[1];
                String c = params[2];

                JSONControl jsControl = new JSONControl();
                JSONObject responseRegister = jsControl.postLoginWP(a, b, c);
                Log.d("json responseRegister", responseRegister.toString());
                ModelUser user = new ModelUser();
                String _id = responseRegister.getString("accid");
                String _name = responseRegister.getString("usrnm");
                String _email = "";
                String _freepoin = responseRegister.getString("freepoin");
                String _token1 = responseRegister.getString("token1");
                String _token2 = responseRegister.getString("token2");
                String _token3 = responseRegister.getString("token3");
                String stat = responseRegister.getString("stat");
                user.setId(_id);
                user.setNama(_name);
                user.setEmail(_email);
                ApplicationManager.getInstance(activity).setUser(user);
                ApplicationManager.getInstance(activity).setFreepoin(_freepoin);
                ApplicationManager.getInstance(activity).setUserToken1(_token1);
                ApplicationManager.getInstance(activity).setUserToken2(_token2);
                ApplicationManager.getInstance(activity).setUserToken3(_token3);
                message = stat;

                if(!stat.contains("Sukses")){
                    return "FAIL";
                }
                else {
                    return "OK";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return "FAIL";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();
            switch (result) {
                case "FAIL":
                    DialogManager.showDialog(mActivity, "Mohon Maaf", message);
                    break;

                case "OK":
                    Intent i = new Intent(getBaseContext(), FreepointMainActivity.class);
                    startActivity(i);
                    finish();
                    break;
            }
        }


    }

    private class DoLoginWG extends AsyncTask<String, Void, String> {
        private String message = "Tidak dapat login";
        private Activity activity;
        private Context context;
        private Resources resources;
        private ProgressDialog progressDialog;
        String error = "";

        public DoLoginWG(Activity activity) {
            super();
            this.activity = activity;
            this.context = activity.getApplicationContext();
            this.resources = activity.getResources();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Signing in. . .");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String a = params[0];
                String b = params[1];
                String c = params[2];

                JSONControl jsControl = new JSONControl();
                JSONObject responseRegister = jsControl.postLoginWG(a, b, c);
                Log.d("json responseRegister", responseRegister.toString());
                if (!responseRegister.toString().contains("errors")) {
                    ModelUser user = new ModelUser();
                    String _id = responseRegister.getString("accid");
                    String _name = responseRegister.getString("usrnm");
                    String _email = "";
                    String _freepoin = responseRegister.getString("freepoin");
                    String _token1 = responseRegister.getString("token1");
                    String _token2 = responseRegister.getString("token2");
                    String _token3 = responseRegister.getString("token3");
                    String stat = responseRegister.getString("stat");
                    user.setId(_id);
                    user.setNama(_name);
                    user.setEmail(_email);
                    ApplicationManager.getInstance(activity).setUser(user);
                    ApplicationManager.getInstance(activity).setFreepoin(_freepoin);
                    ApplicationManager.getInstance(activity).setUserToken1(_token1);
                    ApplicationManager.getInstance(activity).setUserToken2(_token2);
                    ApplicationManager.getInstance(activity).setUserToken3(_token3);
                    message = stat;
                    return "OK";
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return "FAIL";

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progressDialog.dismiss();
            switch (result) {
                case "FAIL":
                    DialogManager.showDialog(mActivity, "Mohon Maaf", message);
                    break;

                case "OK":
                    Intent i = new Intent(getBaseContext(), FreepointMainActivity.class);
                    startActivity(i);
                    finish();
                    break;
            }
        }


    }

}
