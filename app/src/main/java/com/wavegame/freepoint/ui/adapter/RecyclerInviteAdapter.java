package com.wavegame.freepoint.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.viewholder.DummyItemHolder;
import com.wavegame.freepoint.ui.viewholder.InviteItemHolder;

import java.util.List;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class RecyclerInviteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mItemList;

    public RecyclerInviteAdapter(List<String> itemList) {
        mItemList = itemList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View row = LayoutInflater.from(context).inflate(R.layout.recycler_invite_item, parent, false);

        return new InviteItemHolder(row);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        InviteItemHolder vHolder = (InviteItemHolder) holder;
        vHolder.getTv_invite_via().setText("Invite Via "+mItemList.get(position));
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

}
