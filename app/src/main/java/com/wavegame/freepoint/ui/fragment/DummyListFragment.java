package com.wavegame.freepoint.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.wavegame.freepoint.wavegamesdk.helper.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wavegame.freepoint.R;
import com.wavegame.freepoint.ui.adapter.RecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class DummyListFragment extends Fragment {

    private static final String TAG = DummyListFragment.class.getSimpleName();
    public static final String ITEMS_COUNT_KEY = "DummyListFragment$ItemsCount";

    public static DummyListFragment createInstance(int itemsCount) {
        DummyListFragment dummyFragment = new DummyListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ITEMS_COUNT_KEY, itemsCount);
        dummyFragment.setArguments(bundle);
        return dummyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_dummy, container, false);
        setupRecyclerView(recyclerView);
        return recyclerView;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(createItemList());
        recyclerView.setAdapter(recyclerAdapter);
    }

    private List<String> createItemList() {
        List<String> itemList = new ArrayList<>();
        Bundle bundle = getArguments();
        if (bundle != null) {
            int itemsCount = bundle.getInt(ITEMS_COUNT_KEY);
            for (int i = 0; i < itemsCount; i++) {
                itemList.add("Item " + i);
            }
        }
        return itemList;
    }


}
