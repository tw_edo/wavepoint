package com.wavegame.freepoint.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.wavegame.freepoint.R;

/**
 * Created by wavegame9 on 2/27/2016.
 */
public class LaporanItemHolder extends RecyclerView.ViewHolder {
    private final TextView txtItem;

    public LaporanItemHolder(final View parent, TextView textView) {
        super(parent);
        txtItem  = textView;
    }

    public static LaporanItemHolder newInstance(View parent){
        TextView textView = (TextView) parent.findViewById(R.id.txtItem);
        return new LaporanItemHolder(parent, textView);
    }

    public void setTvDummyText(CharSequence text){
        txtItem.setText(text);
    }

}
